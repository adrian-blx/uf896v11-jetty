#!/bin/bash
set -e

rm -f aligned.apk unsigned.apk

java -jar apktool.jar b -o unsigned.apk JETTY
zipalign -v 4 unsigned.apk aligned.apk
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./platform.keystore  aligned.apk testkey
