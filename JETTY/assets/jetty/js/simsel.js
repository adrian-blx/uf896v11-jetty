$(function(){
//	_loadCss("./css/layer.css");
	init();
});

function init(){
	var sim_slot = $.session.get("sim_slot");
	if(sim_slot == 0){
		$("option").removeAttr("selected");
		$("option:eq(0)").attr("selected","selected");
	}else if(sim_slot == 1){
		$("option").removeAttr("selected");
		$("option:eq(1)").attr("selected","selected");
	}else if(sim_slot == 2){
		$("option").removeAttr("selected");
		$("option:eq(2)").attr("selected","selected");
	}
}

$("#select").bind('change', function (e) {
	if($("option:eq(0)").attr("selected") == "selected"){
		$("option").removeAttr("selected");
		$("option:eq(0)").attr("selected","selected");
	}else if($("option:eq(1)").attr("selected") == "selected"){
		$("option").removeAttr("selected");
		$("option:eq(1)").attr("selected","selected");
	}else if($("option:eq(2)").attr("selected") == "selected"){
		$("option").removeAttr("selected");
		$("option:eq(2)").attr("selected","selected");
	}
});

$("#apply").bind('click', function (e) {
	var stat,pwd;
	if($("option:eq(0)").attr("selected") == "selected"){
		stat = $("option:eq(0)").val();
	}else if($("option:eq(1)").attr("selected") == "selected"){
		stat = $("option:eq(1)").val();
	}else if($("option:eq(2)").attr("selected") == "selected"){
		stat = $("option:eq(2)").val();
	}
	pwd = $("#pwd").val();
	var param = {funcNo:1023,sim_slot:stat,pwd:$("#pwd").val()};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			$.session.set("sim_slot", stat);
			return;
		}else{//错误
			Alert(error_info);
		}
	});
});
