function Alert(alertMsg, msgType, yesFunc, btnWords)
{
	var type = msgType==0?1:3; //显示的图标
	iAlertIdx = $.layer({
		area: ['310px','auto'],
		offset: [($(window).height()*0.2)+'px', ''],
		dialog: {
			btn: [btnWords||"Confirm"],
			msg: alertMsg,
			type: type,
			yes: function(index) {try{layer.close(index);}catch(e){}; if(yesFunc){yesFunc();}}
		},
		title: "Warning",
		border: [0 , 0 , '', false],
		shade : [0.5 , '#000' , true],
		success: function(layer) {window.ontouchmove = stopDefaultAction;},
		end: function() {window.ontouchmove = null;}
	});
}

function stopDefaultAction(e)
{
	if(e && e.preventDefault) {
		e.preventDefault();
	} else {
		window.event.returnValue = false;
	}
}
	
function _loadCss(cssAddr)
{
	var aCssAddr = []; // 需要加载的 css 数组
	if(cssAddr instanceof Array)
	{
		aCssAddr = aCssAddr.concat(cssAddr);
	}
	else if(cssAddr)
	{
		aCssAddr.push(cssAddr);
	}
	for(var i = 0, length = aCssAddr.length; i < length; i++)
	{
		var linkDom = document.createElement("link");
		linkDom.charset = 'utf-8';
		linkDom.rel = 'stylesheet';
		linkDom.href = aCssAddr[i];
		var elm = document.querySelector("head");
		//elm.appendChild(linkDom);
		elm.appendChild(linkDom);
	}
}