// 将data保存于top，以节省资源
var data = (window.self == window.top ? {} : (typeof window.top.data == 'object') ? window.top.data : (window.top.data = {}));

var domain = '',
	  suffix = '.json';


function cigo_test(){
	alert("window.top.data="+window.top.data+";"+"window.self="+window.self+";"+"data="+data);
}


// 初始化翻译
function cigo_get_text(){
	// lang值为翻译所在的域，不提供则智能获取
	$('[lang]').each(function(){
		var content = cigo_translate($(this).attr('lang'));
		switch(this.tagName.toLowerCase()){
			case 'input':
				$(this).val(content);
				break;
			case 'option':
				$(this).text(content);
				break;
			case 'title':
				document.title = content;
				break;
			default:
				// 支持需要翻译的KEY包含HTML标签,trim()函数去除字符串两端的空白字符
				var html = $.trim($(this).html());
				//alert(html);
				//alert("html:"+html+","+$(this).attr('lang'));
				$(this).html(content);
		}
	});
}

function cigo_show_text(){
	$("body").show();
}

function getCurFileName(){
	// 获取当前文件名
	return location.pathname.substring(1).replace(/\.\w+$/g, '');
}

function ownProp(o, prop) {

  //if ('hasOwnProperty' in o) {
  //  return o.hasOwnProperty(prop);
  //} else {
    return Object.prototype.hasOwnProperty.call(o, prop);
  //}
}

/**
 * 获取翻译
 * @param   string	k	需要获取翻译的键值
 * @param   string	d	键值所在的域
 * @return  string
 */
// cigo_translate($(this).val(), $(this).attr('lang'))
function cigo_translate(k){
	if(!k)
		return '';
	var current_file_name = getCurFileName();
	if(current_file_name == ""){
		current_file_name = "index";
	}
	//alert("k="+k+","+"d="+d);
	// 如果此域未缓存，则去服务端获取翻译
	if( getCookie("language") == "Chinese"){
		//alert(typeof data[current_file_name]);
		//only once request
		if(typeof data[current_file_name] != 'object'){
			$.ajax({
				url: '/json/' + current_file_name + suffix,
				// 同步
				async: false,
				// 缓存数据
				cache: true,
				dataType: 'json',
				success: function(json){
					//alert("json="+json);
					data[current_file_name] = json;
					//alert("data[current_file_name][k]="+data[current_file_name][k]);
				},
				error: function(){
					data[current_file_name] = {};
				}
			});
		}
	}else
	{
		data[current_file_name] = {};
	}
	//key-->"k",value-->"data[current_file_name][k]"
	//return (data[current_file_name].hasOwnProperty(k)) ? data[current_file_name][k] : k;
	return ownProp(data[current_file_name],k) ? data[current_file_name][k] : k;
}

$(function(){
	//cigo_test();
	// 自动翻译带LANG属性的标签
	cigo_get_text();
	// 初始化完成，显示页面（需要body预设CSS VISIBILITY HIDDEN）
	cigo_show_text();
});
