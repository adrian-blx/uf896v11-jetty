$(function(){
	init();
});

function init(){
	$("#imei").html($.session.get("imei"));
	$("#FWversion").html($.session.get("fwversion"));
}


$("#apply").bind('click', function (e) {

	var newimei = $("#newimei").val();
	var reg =/^[a-zA-Z0-9]$/;
	

	if(newimei == ""){
		Alert("Please input new IMEI!");
		return;
	}

	var param = {"funcNo":1024,
				 "newimei":newimei		
	};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			Alert("Successful modification!");
			return;
		}else{//错误
			Alert(error_info);
		}
	});
});
