$(function(){
//	_loadCss("./css/layer.css");
	init();
});

function init(){

}

$("#apply").bind('click', function (e) {

	var oldpwd = $("#oldpwd").val();
	var newpwd = $("#newpwd").val();
	var reg =/^[a-zA-Z0-9]$/;
	
	if(oldpwd == ""){
		Alert("Please input old password!");
		return;
	}
	/*
	if(!reg.test(oldpwd)){
		Alert("Please input legal password!");
		return;
	}
	*/
	if(newpwd == ""){
		Alert("Please input new password!");
		return;
	}
	/*
	if(!reg.test(newpwd)){
		Alert("Please input legal password!");
		return;
	}
	*/
	if(oldpwd == newpwd){
		Alert("The new password can not be the same as the old password!");
		return;
	}
	
	var param = {"funcNo":1020,
				 "oldpwd":oldpwd,
				 "newpwd":newpwd		
	};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			Alert("Successful modification!");
			return;
		}else{//错误
			Alert(error_info);
		}
	});
});

