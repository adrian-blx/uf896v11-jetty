$(function(){
//	_loadCss("./css/layer.css");
	init();
});

function init(){
	
	var wifi_status,ssid_flag,mode;
	var param = {funcNo:1006};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			var result = data.results[0];
			wifi_status = result.wifi_status;
			ssid_flag = result.ssid_flag;
			mode = result.mode;
			maxSta = result.maxSta;
			initPage(wifi_status,ssid_flag,mode,maxSta);
			if(result.ip){
				$("#ip").html(result.ip);
			}
			if(result.mac){
				$("#mac").html(result.mac);
			}
			if(result.ssid){
				$("#ssid").val(result.ssid);
			}
			if(result.client_num){
				$("#client_num").html(result.client_num   + " /  " + result.maxSta);
			}		
		}else{//错误
			Alert(error_info);
		}
	});
}

function initPage(wifi_status,ssid_flag,mode,maxSta){
	if(wifi_status == "0"){//OFF
		$("#status").html("OFF");
	}
	if(ssid_flag == "0"){//hide
		$("#ssid_nothidden").removeAttr("checked");
		$("#ssid_hidden").attr("checked","checked");
	}
	switch(mode){
		case "b":
			$("#select option").removeAttr("selected");
			$("#select option:eq(0)").attr("selected","selected");
			break;
		case "g-only":
			$("#select option").removeAttr("selected");
			$("#select option:eq(1)").attr("selected","selected");
			break;
		case "n-only":
			$("#select option").removeAttr("selected");
			$("#select option:eq(2)").attr("selected","selected");
			break;
		case "g":
			$("#select option").removeAttr("selected");
			$("#select option:eq(3)").attr("selected","selected");
			break;
		case "n":
			$("#select option").removeAttr("selected");
			$("#select option:eq(4)").attr("selected","selected");
			break;
		default:
			$("#select option").removeAttr("selected");
			$("#select option:eq(0)").attr("selected","selected");
			break;
	}

    switch(maxSta){
        case 1:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(0)").attr("selected","selected");
            break;
        case 2:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(1)").attr("selected","selected");
            break;
        case 3:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(2)").attr("selected","selected");
            break;
        case 4:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(3)").attr("selected","selected");
            break;
        case 5:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(4)").attr("selected","selected");
            break;
        case 6:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(5)").attr("selected","selected");
            break;
        case 7:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(6)").attr("selected","selected");
            break;
        case 8:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(7)").attr("selected","selected");
            break;
        case 9:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(8)").attr("selected","selected");
            break;
        case 10:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(9)").attr("selected","selected");
            break;
        default:
            $("#select2 option").removeAttr("selected");
            $("#select2 option:eq(0)").attr("selected","selected");
            break;
    }
}

$("#select").bind('change', function (e) {
	if($("#select option:eq(0)").attr("selected") == "selected"){
		$("#select option").removeAttr("selected");
		$("#select option:eq(0)").attr("selected","selected");
	}else if($("#select option:eq(1)").attr("selected") == "selected"){
		$("#select option").removeAttr("selected");
		$("#select option:eq(1)").attr("selected","selected");
	}else if($("#select option:eq(2)").attr("selected") == "selected"){
		$("#select option").removeAttr("selected");
		$("#select option:eq(2)").attr("selected","selected");
	}else if($("#select option:eq(3)").attr("selected") == "selected"){
		$("#select option").removeAttr("selected");
		$("#select option:eq(3)").attr("selected","selected");
	}else if($("#select option:eq(4)").attr("selected") == "selected"){
		$("#select option").removeAttr("selected");
		$("#select option:eq(4)").attr("selected","selected");
	}
});


$("#select2").bind('change', function (e) {
	if($("#select2 option:eq(0)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(0)").attr("selected","selected");
	}else if($("#select2 option:eq(1)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(1)").attr("selected","selected");
	}else if($("#select2 option:eq(2)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(2)").attr("selected","selected");
	}else if($("#select2 option:eq(3)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(3)").attr("selected","selected");
	}else if($("#select2 option:eq(4)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(4)").attr("selected","selected");
	}else if($("#select2 option:eq(5)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(5)").attr("selected","selected");
	}else if($("#select2 option:eq(6)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(6)").attr("selected","selected");
	}else if($("#select2 option:eq(7)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(7)").attr("selected","selected");
	}else if($("#select2 option:eq(8)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(8)").attr("selected","selected");
	}else if($("#select2 option:eq(9)").attr("selected") == "selected"){
		$("#select2 option").removeAttr("selected");
		$("#select2 option:eq(9)").attr("selected","selected");
	}
});



$("#ssid_hidden").bind('click', function (e) {
	$("#ssid_nothidden").removeAttr("selected");
	$(this).attr("selected","selected");
});

$("#ssid_nothidden").bind('click', function (e) {
	$("#ssid_hidden").removeAttr("selected");
	$(this).attr("selected","selected");
});

$("#apply").bind('click', function (e) {
	var mod, ssidhidden, maxSta;
	if($("#select option:eq(0)").attr("selected") == "selected"){
		mod = $("#select option:eq(0)").val();
	}else if($("#select option:eq(1)").attr("selected") == "selected"){
		mod = $("#select option:eq(1)").val();
	}else if($("#select option:eq(2)").attr("selected") == "selected"){
		mod = $("#select option:eq(2)").val();
	}else if($("#select option:eq(3)").attr("selected") == "selected"){
		mod = $("#select option:eq(3)").val();
	}else if($("#select option:eq(4)").attr("selected") == "selected"){
		mod = $("#select option:eq(4)").val();
	}

	if($("#select2 option:eq(0)").attr("selected") == "selected"){
		maxSta = $("#select2 option:eq(0)").val();
	}else if($("#select2 option:eq(1)").attr("selected") == "selected"){
		maxSta = $("option:eq(1)").val();
	}else if($("#select2 option:eq(2)").attr("selected") == "selected"){
		maxSta = $("#select2 option:eq(2)").val();
	}else if($("#select2 option:eq(3)").attr("selected") == "selected"){
		maxSta = $("#select2 option:eq(3)").val();
	}else if($("#select2 option:eq(4)").attr("selected") == "selected"){
		maxSta = $("#select2 option:eq(4)").val();
	}else if($("#select2 option:eq(5)").attr("selected") == "selected"){
        maxSta = $("#select2 option:eq(5)").val();
    }else if($("#select2 option:eq(6)").attr("selected") == "selected"){
        maxSta = $("#select2 option:eq(6)").val();
    }else if($("#select2 option:eq(7)").attr("selected") == "selected"){
        maxSta = $("#select2 option:eq(7)").val();
    }else if($("#select2 option:eq(8)").attr("selected") == "selected"){
        maxSta = $("#select2 option:eq(8)").val();
    }else if($("#select2 option:eq(9)").attr("selected") == "selected"){
        maxSta = $("#select2 option:eq(9)").val();
    }
	
	if($("#ssid_hidden").attr("selected") == "selected"){
		ssidhidden = "0";
	}else{
		ssidhidden = "1";
	}
	
	var param = {funcNo:1007,
				ssid_hidden:ssidhidden,
				mode:mod,
				maxSta:maxSta,
				ssid:$("#ssid").val()
	};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			return;
		}else{//错误
			Alert(error_info);
		}
	});
});

$("#restart").bind('click', function (e) {
	var param = {funcNo:1008};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			return;
		}else{//错误
			Alert(error_info);
		}
	});
});
