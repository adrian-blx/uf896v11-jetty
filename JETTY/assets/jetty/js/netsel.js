$(function(){
//	_loadCss("./css/layer.css");
	init();
});

function init(){
	var net_mode = $.session.get("net_mode");
	if(net_mode == 0){
		$("option").removeAttr("selected");
		$("option:eq(0)").attr("selected","selected");
	}else if(net_mode == 1){
		$("option").removeAttr("selected");
		$("option:eq(1)").attr("selected","selected");
	}else if(net_mode == 2){
		$("option").removeAttr("selected");
		$("option:eq(2)").attr("selected","selected");
	}
}

$("#select").bind('change', function (e) {
	if($("option:eq(0)").attr("selected") == "selected"){
		$("option").removeAttr("selected");
		$("option:eq(0)").attr("selected","selected");
	}else if($("option:eq(1)").attr("selected") == "selected"){
		$("option").removeAttr("selected");
		$("option:eq(1)").attr("selected","selected");
	}else if($("option:eq(2)").attr("selected") == "selected"){
		$("option").removeAttr("selected");
		$("option:eq(2)").attr("selected","selected");
	}
});

$("#apply").bind('click', function (e) {
	var stat;
	if($("option:eq(0)").attr("selected") == "selected"){
		stat = $("option:eq(0)").val();
	}else if($("option:eq(1)").attr("selected") == "selected"){
		stat = $("option:eq(1)").val();
	}else if($("option:eq(2)").attr("selected") == "selected"){
		stat = $("option:eq(2)").val();
	}
	var param = {funcNo:1005,net_mode:stat};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			$.session.set("net_mode", stat);
			return;
		}else{//错误
			Alert(error_info);
		}
	});
});
