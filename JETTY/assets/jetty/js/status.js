$(function(){
//	_loadCss("./css/layer.css");
	setInterval(update,5000);
	update();
	simupdate();
	init();
});

function init(){
	var conn_mode = $.session.get("conn_mode");
	if(conn_mode == "1"){//手动
		$("#manual").attr("selected","selected");
		$("#auto").removeAttr("selected");
	}else{//auto
		$("#auto").attr("selected","selected");
		$("#manual").removeAttr("selected");
	}
	var param = {funcNo:1002};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			var result = data.results[0];
			if(result.IP){//IP
				$("#cigo_ip").html(result.IP);
			}
			if(result.mask){//掩码
				$("#g3_mask").html(result.mask);
			}
			if(result.dns){//DNS
				$("#g3_dns").html(result.dns);
			}
			if(result.ssid){//ssid
				$("#ssid").html(result.ssid);
			}
			if(result.wlan_ip){//wlan IP
				$("#wlan_ip").html(result.wlan_ip);
			}		
		}else{//错误
			Alert(error_info);
		}
	});
}

function update(){
	var param = {funcNo:1003};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			var result = data.results[0];

			$("#cigo_tx_data").html(parseInt(result.up_bytes) + " KB");
			$("#cigo_rx_data").html(parseInt(result.down_bytes) + " KB");
			$("#cigo_longtitude").html((result.longtitude));
			$("#cigo_latitude").html((result.latitude));
			$("#client_num").html(result.client_num + " /  " + result.maxSta);

		}else{//错误
			Alert(error_info);
		}
	});
}

function simupdate(){
	var param = {funcNo:1015};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			var result = data.results[0];
			if(result.sim_status){
				$("#simsta").html(result.sim_status);
			}
			if(result.imsi){
				$("#sim_imsi").html(result.imsi);
			}
			if(result.imei){
				$("#sim_imei").html(result.imei);
			}
			if(result.iccid){
				$("#sim_iccid").html(result.iccid);
			}
		}else{//错误
			Alert(error_info);
		}
	});
}

$("#connect").bind('change', function (e) {
	if($("#manual").attr("selected") == "selected"){
		$("#manual").attr("selected","selected");
		$("#auto").removeAttr("selected");
	}else{
		$("#auto").attr("selected","selected");
		$("#manual").removeAttr("selected");
	}
});

$("#apply").bind('click', function (e) {
	var connmode;
	if($("#auto").attr("selected") == "selected"){
		connmode = $("#auto").val();
	}else if($("#manual").attr("selected") == "selected"){
		connmode = $("#manual").val();
	}
	var param = {funcNo:1004,conn_mode:connmode};
	request(param,function(data){
		var flag = data.flag;
		var error_info = data.error_info;
		
		if(flag == "1"){//正确
			$.session.set("conn_mode", connmode);
			return;
		}else{//错误
			Alert(error_info);
		}
	});
});
