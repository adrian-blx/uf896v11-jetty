.class public Lorg/eclipse/jetty/http/HttpStatus;
.super Ljava/lang/Object;
.source "HttpStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/eclipse/jetty/http/HttpStatus$Code;
    }
.end annotation


# static fields
.field private static final codeMap:[Lorg/eclipse/jetty/http/HttpStatus$Code;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 670
    const/16 v4, 0x1fc

    new-array v4, v4, [Lorg/eclipse/jetty/http/HttpStatus$Code;

    sput-object v4, Lorg/eclipse/jetty/http/HttpStatus;->codeMap:[Lorg/eclipse/jetty/http/HttpStatus$Code;

    .line 674
    invoke-static {}, Lorg/eclipse/jetty/http/HttpStatus$Code;->values()[Lorg/eclipse/jetty/http/HttpStatus$Code;

    move-result-object v0

    .local v0, "arr$":[Lorg/eclipse/jetty/http/HttpStatus$Code;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 676
    .local v1, "code":Lorg/eclipse/jetty/http/HttpStatus$Code;
    sget-object v4, Lorg/eclipse/jetty/http/HttpStatus;->codeMap:[Lorg/eclipse/jetty/http/HttpStatus$Code;

    invoke-static {v1}, Lorg/eclipse/jetty/http/HttpStatus$Code;->access$000(Lorg/eclipse/jetty/http/HttpStatus$Code;)I

    move-result v5

    aput-object v1, v4, v5

    .line 674
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 678
    .end local v1    # "code":Lorg/eclipse/jetty/http/HttpStatus$Code;
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 681
    return-void
.end method

.method public static getCode(I)Lorg/eclipse/jetty/http/HttpStatus$Code;
    .locals 1
    .param p0, "code"    # I

    .prologue
    .line 929
    const/16 v0, 0x1fb

    if-gt p0, v0, :cond_0

    .line 931
    sget-object v0, Lorg/eclipse/jetty/http/HttpStatus;->codeMap:[Lorg/eclipse/jetty/http/HttpStatus$Code;

    aget-object v0, v0, p0

    .line 933
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getMessage(I)Ljava/lang/String;
    .locals 2
    .param p0, "code"    # I

    .prologue
    .line 946
    invoke-static {p0}, Lorg/eclipse/jetty/http/HttpStatus;->getCode(I)Lorg/eclipse/jetty/http/HttpStatus$Code;

    move-result-object v0

    .line 947
    .local v0, "codeEnum":Lorg/eclipse/jetty/http/HttpStatus$Code;
    if-eqz v0, :cond_0

    .line 949
    invoke-virtual {v0}, Lorg/eclipse/jetty/http/HttpStatus$Code;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 953
    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
