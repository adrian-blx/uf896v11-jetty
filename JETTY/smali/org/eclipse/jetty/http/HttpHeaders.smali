.class public Lorg/eclipse/jetty/http/HttpHeaders;
.super Lorg/eclipse/jetty/io/BufferCache;
.source "HttpHeaders.java"


# static fields
.field public static final ACCEPT_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final ACCEPT_CHARSET_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final ACCEPT_ENCODING_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final ACCEPT_LANGUAGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final ACCEPT_RANGES_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final AGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final ALLOW_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final AUTHORIZATION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

.field public static final CACHE_CONTROL_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final CONNECTION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final CONTENT_ENCODING_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final CONTENT_LANGUAGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final CONTENT_LENGTH_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final CONTENT_LOCATION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final CONTENT_MD5_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final CONTENT_RANGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final CONTENT_TYPE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final COOKIE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final DATE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final ETAG_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final EXPECT_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final EXPIRES_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final FORWARDED_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final FROM_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final HOST_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final IDENTITY_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final IF_MATCH_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final IF_MODIFIED_SINCE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final IF_NONE_MATCH_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final IF_RANGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final IF_UNMODIFIED_SINCE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final KEEP_ALIVE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final LAST_MODIFIED_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final LOCATION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final MAX_FORWARDS_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final MIME_VERSION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final PRAGMA_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final PROXY_AUTHENTICATE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final PROXY_AUTHORIZATION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final PROXY_CONNECTION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final RANGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final REFERER_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final REQUEST_RANGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final RETRY_AFTER_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final SERVER_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final SERVLET_ENGINE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final SET_COOKIE2_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final SET_COOKIE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final TE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final TRAILER_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final TRANSFER_ENCODING_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final UPGRADE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final USER_AGENT_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final VARY_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final VIA_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final WARNING_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final WWW_AUTHENTICATE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final X_FORWARDED_FOR_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final X_FORWARDED_HOST_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final X_FORWARDED_PROTO_BUFFER:Lorg/eclipse/jetty/io/Buffer;

.field public static final X_FORWARDED_SERVER_BUFFER:Lorg/eclipse/jetty/io/Buffer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 174
    new-instance v0, Lorg/eclipse/jetty/http/HttpHeaders;

    invoke-direct {v0}, Lorg/eclipse/jetty/http/HttpHeaders;-><init>()V

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    .line 177
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Host"

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->HOST_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 178
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Accept"

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->ACCEPT_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 179
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Accept-Charset"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->ACCEPT_CHARSET_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 180
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Accept-Encoding"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->ACCEPT_ENCODING_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 181
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Accept-Language"

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->ACCEPT_LANGUAGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 183
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Content-Length"

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CONTENT_LENGTH_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 184
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Connection"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CONNECTION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 185
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Cache-Control"

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE_CONTROL_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 186
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Date"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->DATE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 187
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Pragma"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->PRAGMA_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 188
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Trailer"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->TRAILER_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 189
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Transfer-Encoding"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->TRANSFER_ENCODING_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 190
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Upgrade"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->UPGRADE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 191
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Via"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->VIA_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 192
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Warning"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->WARNING_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 193
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Allow"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->ALLOW_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 194
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Content-Encoding"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CONTENT_ENCODING_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 195
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Content-Language"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CONTENT_LANGUAGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 196
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Content-Location"

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CONTENT_LOCATION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 197
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Content-MD5"

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CONTENT_MD5_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 198
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Content-Range"

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CONTENT_RANGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 199
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Content-Type"

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CONTENT_TYPE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 200
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Expires"

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->EXPIRES_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 201
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Last-Modified"

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->LAST_MODIFIED_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 202
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Authorization"

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->AUTHORIZATION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 203
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Expect"

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->EXPECT_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 204
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Forwarded"

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->FORWARDED_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 205
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "From"

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->FROM_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 206
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "If-Match"

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->IF_MATCH_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 207
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "If-Modified-Since"

    const/16 v2, 0x1d

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->IF_MODIFIED_SINCE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 208
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "If-None-Match"

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->IF_NONE_MATCH_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 209
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "If-Range"

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->IF_RANGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 210
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "If-Unmodified-Since"

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->IF_UNMODIFIED_SINCE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 211
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Keep-Alive"

    const/16 v2, 0x21

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->KEEP_ALIVE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 212
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Max-Forwards"

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->MAX_FORWARDS_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 213
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Proxy-Authorization"

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->PROXY_AUTHORIZATION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 214
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Range"

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->RANGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 215
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Request-Range"

    const/16 v2, 0x25

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->REQUEST_RANGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 216
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Referer"

    const/16 v2, 0x26

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->REFERER_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 217
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "TE"

    const/16 v2, 0x27

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->TE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 218
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "User-Agent"

    const/16 v2, 0x28

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->USER_AGENT_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 219
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "X-Forwarded-For"

    const/16 v2, 0x29

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->X_FORWARDED_FOR_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 220
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "X-Forwarded-Proto"

    const/16 v2, 0x3b

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->X_FORWARDED_PROTO_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 221
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "X-Forwarded-Server"

    const/16 v2, 0x3c

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->X_FORWARDED_SERVER_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 222
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "X-Forwarded-Host"

    const/16 v2, 0x3d

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->X_FORWARDED_HOST_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 223
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Accept-Ranges"

    const/16 v2, 0x2a

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->ACCEPT_RANGES_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 224
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Age"

    const/16 v2, 0x2b

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->AGE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 225
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "ETag"

    const/16 v2, 0x2c

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->ETAG_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 226
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Location"

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->LOCATION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 227
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Proxy-Authenticate"

    const/16 v2, 0x2e

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->PROXY_AUTHENTICATE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 228
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Retry-After"

    const/16 v2, 0x2f

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->RETRY_AFTER_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 229
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Server"

    const/16 v2, 0x30

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->SERVER_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 230
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Servlet-Engine"

    const/16 v2, 0x31

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->SERVLET_ENGINE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 231
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Vary"

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->VARY_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 232
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "WWW-Authenticate"

    const/16 v2, 0x33

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->WWW_AUTHENTICATE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 233
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Cookie"

    const/16 v2, 0x34

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->COOKIE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 234
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Set-Cookie"

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->SET_COOKIE_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 235
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Set-Cookie2"

    const/16 v2, 0x36

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->SET_COOKIE2_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 236
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "MIME-Version"

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->MIME_VERSION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 237
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "identity"

    const/16 v2, 0x38

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->IDENTITY_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    .line 238
    sget-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->CACHE:Lorg/eclipse/jetty/http/HttpHeaders;

    const-string v1, "Proxy-Connection"

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2}, Lorg/eclipse/jetty/http/HttpHeaders;->add(Ljava/lang/String;I)Lorg/eclipse/jetty/io/BufferCache$CachedBuffer;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/http/HttpHeaders;->PROXY_CONNECTION_BUFFER:Lorg/eclipse/jetty/io/Buffer;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/eclipse/jetty/io/BufferCache;-><init>()V

    return-void
.end method
