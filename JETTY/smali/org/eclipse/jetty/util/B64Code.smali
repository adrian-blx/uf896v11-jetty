.class public Lorg/eclipse/jetty/util/B64Code;
.super Ljava/lang/Object;
.source "B64Code.java"


# static fields
.field static final __rfc1421alphabet:[C

.field static final __rfc1421nibbles:[B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x100

    const/16 v4, 0x40

    .line 38
    new-array v2, v4, [C

    fill-array-data v2, :array_0

    sput-object v2, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    .line 50
    new-array v2, v5, [B

    sput-object v2, Lorg/eclipse/jetty/util/B64Code;->__rfc1421nibbles:[B

    .line 51
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_0

    .line 52
    sget-object v2, Lorg/eclipse/jetty/util/B64Code;->__rfc1421nibbles:[B

    const/4 v3, -0x1

    aput-byte v3, v2, v1

    .line 51
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53
    :cond_0
    const/4 v0, 0x0

    .local v0, "b":B
    :goto_1
    if-ge v0, v4, :cond_1

    .line 54
    sget-object v2, Lorg/eclipse/jetty/util/B64Code;->__rfc1421nibbles:[B

    sget-object v3, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    aget-char v3, v3, v0

    int-to-byte v3, v3

    aput-byte v0, v2, v3

    .line 53
    add-int/lit8 v2, v0, 0x1

    int-to-byte v0, v2

    goto :goto_1

    .line 55
    :cond_1
    sget-object v2, Lorg/eclipse/jetty/util/B64Code;->__rfc1421nibbles:[B

    const/16 v3, 0x3d

    const/4 v4, 0x0

    aput-byte v4, v2, v3

    .line 56
    return-void

    .line 38
    :array_0
    .array-data 2
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
        0x47s
        0x48s
        0x49s
        0x4as
        0x4bs
        0x4cs
        0x4ds
        0x4es
        0x4fs
        0x50s
        0x51s
        0x52s
        0x53s
        0x54s
        0x55s
        0x56s
        0x57s
        0x58s
        0x59s
        0x5as
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
        0x67s
        0x68s
        0x69s
        0x6as
        0x6bs
        0x6cs
        0x6ds
        0x6es
        0x6fs
        0x70s
        0x71s
        0x72s
        0x73s
        0x74s
        0x75s
        0x76s
        0x77s
        0x78s
        0x79s
        0x7as
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x2bs
        0x2fs
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "encoded"    # Ljava/lang/String;
    .param p1, "charEncoding"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 246
    invoke-static {p0}, Lorg/eclipse/jetty/util/B64Code;->decode(Ljava/lang/String;)[B

    move-result-object v0

    .line 247
    .local v0, "decoded":[B
    if-nez p1, :cond_0

    .line 248
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 249
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_0
.end method

.method public static decode(Ljava/lang/String;Ljava/io/ByteArrayOutputStream;)V
    .locals 11
    .param p0, "encoded"    # Ljava/lang/String;
    .param p1, "bout"    # Ljava/io/ByteArrayOutputStream;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 371
    if-nez p0, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    if-nez p1, :cond_2

    .line 375
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "No outputstream for decoded bytes"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 377
    :cond_2
    const/4 v1, 0x0

    .line 378
    .local v1, "ci":I
    const/4 v7, 0x4

    new-array v4, v7, [B

    .line 379
    .local v4, "nibbles":[B
    const/4 v5, 0x0

    .line 381
    .local v5, "s":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 383
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "ci":I
    .local v2, "ci":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 385
    .local v0, "c":C
    const/16 v7, 0x3d

    if-ne v0, v7, :cond_3

    move v1, v2

    .line 386
    .end local v2    # "ci":I
    .restart local v1    # "ci":I
    goto :goto_0

    .line 388
    .end local v1    # "ci":I
    .restart local v2    # "ci":I
    :cond_3
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v7

    if-eqz v7, :cond_4

    move v1, v2

    .line 389
    .end local v2    # "ci":I
    .restart local v1    # "ci":I
    goto :goto_1

    .line 391
    .end local v1    # "ci":I
    .restart local v2    # "ci":I
    :cond_4
    sget-object v7, Lorg/eclipse/jetty/util/B64Code;->__rfc1421nibbles:[B

    aget-byte v3, v7, v0

    .line 392
    .local v3, "nibble":B
    if-gez v3, :cond_5

    .line 393
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Not B64 encoded"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 395
    :cond_5
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "s":I
    .local v6, "s":I
    sget-object v7, Lorg/eclipse/jetty/util/B64Code;->__rfc1421nibbles:[B

    aget-byte v7, v7, v0

    aput-byte v7, v4, v5

    .line 397
    packed-switch v6, :pswitch_data_0

    move v5, v6

    .end local v6    # "s":I
    .restart local v5    # "s":I
    :goto_2
    move v1, v2

    .line 413
    .end local v2    # "ci":I
    .restart local v1    # "ci":I
    goto :goto_1

    .end local v1    # "ci":I
    .end local v5    # "s":I
    .restart local v2    # "ci":I
    .restart local v6    # "s":I
    :pswitch_0
    move v5, v6

    .line 400
    .end local v6    # "s":I
    .restart local v5    # "s":I
    goto :goto_2

    .line 402
    .end local v5    # "s":I
    .restart local v6    # "s":I
    :pswitch_1
    const/4 v7, 0x0

    aget-byte v7, v4, v7

    shl-int/lit8 v7, v7, 0x2

    aget-byte v8, v4, v9

    ushr-int/lit8 v8, v8, 0x4

    or-int/2addr v7, v8

    invoke-virtual {p1, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    move v5, v6

    .line 403
    .end local v6    # "s":I
    .restart local v5    # "s":I
    goto :goto_2

    .line 405
    .end local v5    # "s":I
    .restart local v6    # "s":I
    :pswitch_2
    aget-byte v7, v4, v9

    shl-int/lit8 v7, v7, 0x4

    aget-byte v8, v4, v10

    ushr-int/lit8 v8, v8, 0x2

    or-int/2addr v7, v8

    invoke-virtual {p1, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    move v5, v6

    .line 406
    .end local v6    # "s":I
    .restart local v5    # "s":I
    goto :goto_2

    .line 408
    .end local v5    # "s":I
    .restart local v6    # "s":I
    :pswitch_3
    aget-byte v7, v4, v10

    shl-int/lit8 v7, v7, 0x6

    const/4 v8, 0x3

    aget-byte v8, v4, v8

    or-int/2addr v7, v8

    invoke-virtual {p1, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 409
    const/4 v5, 0x0

    .end local v6    # "s":I
    .restart local v5    # "s":I
    goto :goto_2

    .line 397
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static decode(Ljava/lang/String;)[B
    .locals 2
    .param p0, "encoded"    # Ljava/lang/String;

    .prologue
    .line 351
    if-nez p0, :cond_0

    .line 352
    const/4 v1, 0x0

    .line 356
    :goto_0
    return-object v1

    .line 354
    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    div-int/lit8 v1, v1, 0x3

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 355
    .local v0, "bout":Ljava/io/ByteArrayOutputStream;
    invoke-static {p0, v0}, Lorg/eclipse/jetty/util/B64Code;->decode(Ljava/lang/String;Ljava/io/ByteArrayOutputStream;)V

    .line 356
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    goto :goto_0
.end method

.method public static encode(JLjava/lang/Appendable;)V
    .locals 5
    .param p0, "lvalue"    # J
    .param p2, "buf"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 434
    const-wide/16 v1, -0x4

    const/16 v3, 0x20

    shr-long v3, p0, v3

    and-long/2addr v1, v3

    long-to-int v0, v1

    .line 435
    .local v0, "value":I
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    const/high16 v2, -0x4000000

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x1a

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 436
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    const/high16 v2, 0x3f00000

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x14

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 437
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    const v2, 0xfc000

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0xe

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 438
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    and-int/lit16 v2, v0, 0x3f00

    shr-int/lit8 v2, v2, 0x8

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 439
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    and-int/lit16 v2, v0, 0xfc

    shr-int/lit8 v2, v2, 0x2

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 441
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    and-int/lit8 v2, v0, 0x3

    shl-int/lit8 v2, v2, 0x4

    const/16 v3, 0x1c

    shr-long v3, p0, v3

    long-to-int v3, v3

    and-int/lit8 v3, v3, 0xf

    add-int/2addr v2, v3

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 443
    const v1, 0xfffffff

    long-to-int v2, p0

    and-int v0, v1, v2

    .line 444
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    const/high16 v2, 0xfc00000

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x16

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 445
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    const/high16 v2, 0x3f0000

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x10

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 446
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    const v2, 0xfc00

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0xa

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 447
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    and-int/lit16 v2, v0, 0x3f0

    shr-int/lit8 v2, v2, 0x4

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 448
    sget-object v1, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    and-int/lit8 v2, v0, 0xf

    shl-int/lit8 v2, v2, 0x2

    and-int/lit8 v2, v2, 0x3f

    aget-char v1, v1, v2

    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 449
    return-void
.end method

.method public static encode([B)[C
    .locals 15
    .param p0, "b"    # [B

    .prologue
    const/16 v14, 0x3d

    .line 108
    if-nez p0, :cond_0

    .line 109
    const/4 v6, 0x0

    .line 155
    :goto_0
    return-object v6

    .line 111
    :cond_0
    array-length v3, p0

    .line 112
    .local v3, "bLen":I
    add-int/lit8 v11, v3, 0x2

    div-int/lit8 v11, v11, 0x3

    mul-int/lit8 v7, v11, 0x4

    .line 113
    .local v7, "cLen":I
    new-array v6, v7, [C

    .line 114
    .local v6, "c":[C
    const/4 v8, 0x0

    .line 115
    .local v8, "ci":I
    const/4 v4, 0x0

    .line 117
    .local v4, "bi":I
    div-int/lit8 v11, v3, 0x3

    mul-int/lit8 v10, v11, 0x3

    .local v10, "stop":I
    move v5, v4

    .end local v4    # "bi":I
    .local v5, "bi":I
    move v9, v8

    .line 118
    .end local v8    # "ci":I
    .local v9, "ci":I
    :goto_1
    if-ge v5, v10, :cond_1

    .line 120
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "bi":I
    .restart local v4    # "bi":I
    aget-byte v0, p0, v5

    .line 121
    .local v0, "b0":B
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "bi":I
    .restart local v5    # "bi":I
    aget-byte v1, p0, v4

    .line 122
    .local v1, "b1":B
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "bi":I
    .restart local v4    # "bi":I
    aget-byte v2, p0, v5

    .line 123
    .local v2, "b2":B
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "ci":I
    .restart local v8    # "ci":I
    sget-object v11, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    ushr-int/lit8 v12, v0, 0x2

    and-int/lit8 v12, v12, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v9

    .line 124
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "ci":I
    .restart local v9    # "ci":I
    sget-object v11, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    shl-int/lit8 v12, v0, 0x4

    and-int/lit8 v12, v12, 0x3f

    ushr-int/lit8 v13, v1, 0x4

    and-int/lit8 v13, v13, 0xf

    or-int/2addr v12, v13

    aget-char v11, v11, v12

    aput-char v11, v6, v8

    .line 125
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "ci":I
    .restart local v8    # "ci":I
    sget-object v11, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    shl-int/lit8 v12, v1, 0x2

    and-int/lit8 v12, v12, 0x3f

    ushr-int/lit8 v13, v2, 0x6

    and-int/lit8 v13, v13, 0x3

    or-int/2addr v12, v13

    aget-char v11, v11, v12

    aput-char v11, v6, v9

    .line 126
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "ci":I
    .restart local v9    # "ci":I
    sget-object v11, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    and-int/lit8 v12, v2, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v8

    move v5, v4

    .end local v4    # "bi":I
    .restart local v5    # "bi":I
    goto :goto_1

    .line 129
    .end local v0    # "b0":B
    .end local v1    # "b1":B
    .end local v2    # "b2":B
    :cond_1
    if-eq v3, v5, :cond_2

    .line 131
    rem-int/lit8 v11, v3, 0x3

    packed-switch v11, :pswitch_data_0

    :cond_2
    move v4, v5

    .end local v5    # "bi":I
    .restart local v4    # "bi":I
    move v8, v9

    .end local v9    # "ci":I
    .restart local v8    # "ci":I
    goto :goto_0

    .line 134
    .end local v4    # "bi":I
    .end local v8    # "ci":I
    .restart local v5    # "bi":I
    .restart local v9    # "ci":I
    :pswitch_0
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "bi":I
    .restart local v4    # "bi":I
    aget-byte v0, p0, v5

    .line 135
    .restart local v0    # "b0":B
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "bi":I
    .restart local v5    # "bi":I
    aget-byte v1, p0, v4

    .line 136
    .restart local v1    # "b1":B
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "ci":I
    .restart local v8    # "ci":I
    sget-object v11, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    ushr-int/lit8 v12, v0, 0x2

    and-int/lit8 v12, v12, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v9

    .line 137
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "ci":I
    .restart local v9    # "ci":I
    sget-object v11, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    shl-int/lit8 v12, v0, 0x4

    and-int/lit8 v12, v12, 0x3f

    ushr-int/lit8 v13, v1, 0x4

    and-int/lit8 v13, v13, 0xf

    or-int/2addr v12, v13

    aget-char v11, v11, v12

    aput-char v11, v6, v8

    .line 138
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "ci":I
    .restart local v8    # "ci":I
    sget-object v11, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    shl-int/lit8 v12, v1, 0x2

    and-int/lit8 v12, v12, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v9

    .line 139
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "ci":I
    .restart local v9    # "ci":I
    aput-char v14, v6, v8

    move v4, v5

    .end local v5    # "bi":I
    .restart local v4    # "bi":I
    move v8, v9

    .line 140
    .end local v9    # "ci":I
    .restart local v8    # "ci":I
    goto/16 :goto_0

    .line 143
    .end local v0    # "b0":B
    .end local v1    # "b1":B
    .end local v4    # "bi":I
    .end local v8    # "ci":I
    .restart local v5    # "bi":I
    .restart local v9    # "ci":I
    :pswitch_1
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "bi":I
    .restart local v4    # "bi":I
    aget-byte v0, p0, v5

    .line 144
    .restart local v0    # "b0":B
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "ci":I
    .restart local v8    # "ci":I
    sget-object v11, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    ushr-int/lit8 v12, v0, 0x2

    and-int/lit8 v12, v12, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v9

    .line 145
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "ci":I
    .restart local v9    # "ci":I
    sget-object v11, Lorg/eclipse/jetty/util/B64Code;->__rfc1421alphabet:[C

    shl-int/lit8 v12, v0, 0x4

    and-int/lit8 v12, v12, 0x3f

    aget-char v11, v11, v12

    aput-char v11, v6, v8

    .line 146
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "ci":I
    .restart local v8    # "ci":I
    aput-char v14, v6, v9

    .line 147
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "ci":I
    .restart local v9    # "ci":I
    aput-char v14, v6, v8

    move v8, v9

    .line 148
    .end local v9    # "ci":I
    .restart local v8    # "ci":I
    goto/16 :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
