.class public Lorg/eclipse/jetty/util/ByteArrayOutputStream2;
.super Ljava/io/ByteArrayOutputStream;
.source "ByteArrayOutputStream2.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getBuf()[B
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lorg/eclipse/jetty/util/ByteArrayOutputStream2;->buf:[B

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lorg/eclipse/jetty/util/ByteArrayOutputStream2;->count:I

    return v0
.end method

.method public setCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 33
    iput p1, p0, Lorg/eclipse/jetty/util/ByteArrayOutputStream2;->count:I

    return-void
.end method
