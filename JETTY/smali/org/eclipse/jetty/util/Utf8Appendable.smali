.class public abstract Lorg/eclipse/jetty/util/Utf8Appendable;
.super Ljava/lang/Object;
.source "Utf8Appendable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/eclipse/jetty/util/Utf8Appendable$NotUtf8Exception;
    }
.end annotation


# static fields
.field private static final BYTE_TABLE:[B

.field protected static final LOG:Lorg/eclipse/jetty/util/log/Logger;

.field private static final TRANS_TABLE:[B


# instance fields
.field protected final _appendable:Ljava/lang/Appendable;

.field private _codep:I

.field protected _state:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lorg/eclipse/jetty/util/Utf8Appendable;

    invoke-static {v0}, Lorg/eclipse/jetty/util/log/Log;->getLogger(Ljava/lang/Class;)Lorg/eclipse/jetty/util/log/Logger;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/util/Utf8Appendable;->LOG:Lorg/eclipse/jetty/util/log/Logger;

    .line 60
    const/16 v0, 0x100

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/eclipse/jetty/util/Utf8Appendable;->BYTE_TABLE:[B

    .line 74
    const/16 v0, 0x6c

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lorg/eclipse/jetty/util/Utf8Appendable;->TRANS_TABLE:[B

    return-void

    .line 60
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x7t
        0x8t
        0x8t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0xat
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x4t
        0x3t
        0x3t
        0xbt
        0x6t
        0x6t
        0x6t
        0x5t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
    .end array-data

    .line 74
    :array_1
    .array-data 1
        0x0t
        0xct
        0x18t
        0x24t
        0x3ct
        0x60t
        0x54t
        0xct
        0xct
        0xct
        0x30t
        0x48t
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0x0t
        0xct
        0xct
        0xct
        0xct
        0xct
        0x0t
        0xct
        0x0t
        0xct
        0xct
        0xct
        0x18t
        0xct
        0xct
        0xct
        0xct
        0xct
        0x18t
        0xct
        0x18t
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0x18t
        0xct
        0xct
        0xct
        0xct
        0xct
        0x18t
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0x18t
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0x24t
        0xct
        0x24t
        0xct
        0xct
        0xct
        0x24t
        0xct
        0xct
        0xct
        0xct
        0xct
        0x24t
        0xct
        0x24t
        0xct
        0xct
        0xct
        0x24t
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "appendable"    # Ljava/lang/Appendable;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    .line 89
    iput-object p1, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_appendable:Ljava/lang/Appendable;

    .line 90
    return-void
.end method


# virtual methods
.method public append(B)V
    .locals 2
    .param p1, "b"    # B

    .prologue
    .line 103
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/eclipse/jetty/util/Utf8Appendable;->appendByte(B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public append([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 115
    add-int v1, p2, p3

    .line 116
    .local v1, "end":I
    move v2, p2

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 117
    :try_start_0
    aget-byte v3, p1, v2

    invoke-virtual {p0, v3}, Lorg/eclipse/jetty/util/Utf8Appendable;->appendByte(B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 123
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    return-void
.end method

.method protected appendByte(B)V
    .locals 11
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 147
    if-lez p1, :cond_1

    iget v8, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    if-nez v8, :cond_1

    .line 149
    iget-object v8, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_appendable:Ljava/lang/Appendable;

    and-int/lit16 v9, p1, 0xff

    int-to-char v9, v9

    invoke-interface {v8, v9}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    and-int/lit16 v2, p1, 0xff

    .line 154
    .local v2, "i":I
    sget-object v8, Lorg/eclipse/jetty/util/Utf8Appendable;->BYTE_TABLE:[B

    aget-byte v7, v8, v2

    .line 155
    .local v7, "type":I
    iget v8, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    if-nez v8, :cond_2

    const/16 v8, 0xff

    shr-int/2addr v8, v7

    and-int/2addr v8, v2

    :goto_1
    iput v8, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_codep:I

    .line 156
    sget-object v8, Lorg/eclipse/jetty/util/Utf8Appendable;->TRANS_TABLE:[B

    iget v9, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    add-int/2addr v9, v7

    aget-byte v5, v8, v9

    .line 158
    .local v5, "next":I
    sparse-switch v5, :sswitch_data_0

    .line 181
    iput v5, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    goto :goto_0

    .line 155
    .end local v5    # "next":I
    :cond_2
    and-int/lit8 v8, v2, 0x3f

    iget v9, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_codep:I

    shl-int/lit8 v9, v9, 0x6

    or-int/2addr v8, v9

    goto :goto_1

    .line 161
    .restart local v5    # "next":I
    :sswitch_0
    iput v5, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    .line 162
    iget v8, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_codep:I

    const v9, 0xd800

    if-ge v8, v9, :cond_3

    .line 164
    iget-object v8, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_appendable:Ljava/lang/Appendable;

    iget v9, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_codep:I

    int-to-char v9, v9

    invoke-interface {v8, v9}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    goto :goto_0

    .line 168
    :cond_3
    iget v8, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_codep:I

    invoke-static {v8}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2
    if-ge v3, v4, :cond_0

    aget-char v1, v0, v3

    .line 169
    .local v1, "c":C
    iget-object v8, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_appendable:Ljava/lang/Appendable;

    invoke-interface {v8, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 168
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 174
    .end local v0    # "arr$":[C
    .end local v1    # "c":C
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :sswitch_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "byte "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p1}, Lorg/eclipse/jetty/util/TypeUtil;->toHexString(B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " in state "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    div-int/lit8 v9, v9, 0xc

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 175
    .local v6, "reason":Ljava/lang/String;
    iput v10, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_codep:I

    .line 176
    iput v10, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    .line 177
    iget-object v8, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_appendable:Ljava/lang/Appendable;

    const v9, 0xfffd

    invoke-interface {v8, v9}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 178
    new-instance v8, Lorg/eclipse/jetty/util/Utf8Appendable$NotUtf8Exception;

    invoke-direct {v8, v6}, Lorg/eclipse/jetty/util/Utf8Appendable$NotUtf8Exception;-><init>(Ljava/lang/String;)V

    throw v8

    .line 158
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xc -> :sswitch_1
    .end sparse-switch
.end method

.method protected checkState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 202
    invoke-virtual {p0}, Lorg/eclipse/jetty/util/Utf8Appendable;->isUtf8SequenceComplete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 204
    iput v2, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_codep:I

    .line 205
    iput v2, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    .line 208
    :try_start_0
    iget-object v1, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_appendable:Ljava/lang/Appendable;

    const v2, 0xfffd

    invoke-interface {v1, v2}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    new-instance v1, Lorg/eclipse/jetty/util/Utf8Appendable$NotUtf8Exception;

    const-string v2, "incomplete UTF8 sequence"

    invoke-direct {v1, v2}, Lorg/eclipse/jetty/util/Utf8Appendable$NotUtf8Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 210
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 216
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    return-void
.end method

.method public isUtf8SequenceComplete()Z
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected reset()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    .line 97
    return-void
.end method

.method public toReplacedString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 220
    invoke-virtual {p0}, Lorg/eclipse/jetty/util/Utf8Appendable;->isUtf8SequenceComplete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 222
    iput v4, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_codep:I

    .line 223
    iput v4, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_state:I

    .line 226
    :try_start_0
    iget-object v2, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_appendable:Ljava/lang/Appendable;

    const v3, 0xfffd

    invoke-interface {v2, v3}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    new-instance v1, Lorg/eclipse/jetty/util/Utf8Appendable$NotUtf8Exception;

    const-string v2, "incomplete UTF8 sequence"

    invoke-direct {v1, v2}, Lorg/eclipse/jetty/util/Utf8Appendable$NotUtf8Exception;-><init>(Ljava/lang/String;)V

    .line 233
    .local v1, "th":Ljava/lang/Throwable;
    sget-object v2, Lorg/eclipse/jetty/util/Utf8Appendable;->LOG:Lorg/eclipse/jetty/util/log/Logger;

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {v2, v3, v4}, Lorg/eclipse/jetty/util/log/Logger;->warn(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    sget-object v2, Lorg/eclipse/jetty/util/Utf8Appendable;->LOG:Lorg/eclipse/jetty/util/log/Logger;

    invoke-interface {v2, v1}, Lorg/eclipse/jetty/util/log/Logger;->debug(Ljava/lang/Throwable;)V

    .line 236
    .end local v1    # "th":Ljava/lang/Throwable;
    :cond_0
    iget-object v2, p0, Lorg/eclipse/jetty/util/Utf8Appendable;->_appendable:Ljava/lang/Appendable;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 228
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method
