.class public Lorg/eclipse/jetty/util/security/Password;
.super Lorg/eclipse/jetty/util/security/Credential;
.source "Password.java"


# static fields
.field private static final LOG:Lorg/eclipse/jetty/util/log/Logger;

.field private static final serialVersionUID:J = 0x46430ecd65ae3425L


# instance fields
.field private _pw:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lorg/eclipse/jetty/util/security/Password;

    invoke-static {v0}, Lorg/eclipse/jetty/util/log/Log;->getLogger(Ljava/lang/Class;)Lorg/eclipse/jetty/util/log/Logger;

    move-result-object v0

    sput-object v0, Lorg/eclipse/jetty/util/security/Password;->LOG:Lorg/eclipse/jetty/util/log/Logger;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 114
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 130
    :cond_0
    :goto_0
    return v1

    .line 117
    :cond_1
    if-eqz p1, :cond_0

    .line 120
    instance-of v3, p1, Lorg/eclipse/jetty/util/security/Password;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 122
    check-cast v0, Lorg/eclipse/jetty/util/security/Password;

    .line 124
    .local v0, "p":Lorg/eclipse/jetty/util/security/Password;
    iget-object v3, v0, Lorg/eclipse/jetty/util/security/Password;->_pw:Ljava/lang/String;

    iget-object v4, p0, Lorg/eclipse/jetty/util/security/Password;->_pw:Ljava/lang/String;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lorg/eclipse/jetty/util/security/Password;->_pw:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/eclipse/jetty/util/security/Password;->_pw:Ljava/lang/String;

    iget-object v4, v0, Lorg/eclipse/jetty/util/security/Password;->_pw:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 127
    .end local v0    # "p":Lorg/eclipse/jetty/util/security/Password;
    :cond_3
    instance-of v2, p1, Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 128
    iget-object v1, p0, Lorg/eclipse/jetty/util/security/Password;->_pw:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lorg/eclipse/jetty/util/security/Password;->_pw:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/eclipse/jetty/util/security/Password;->_pw:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/eclipse/jetty/util/security/Password;->_pw:Ljava/lang/String;

    return-object v0
.end method
