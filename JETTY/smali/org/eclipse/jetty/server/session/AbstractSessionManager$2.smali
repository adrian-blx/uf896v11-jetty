.class Lorg/eclipse/jetty/server/session/AbstractSessionManager$2;
.super Ljava/lang/Object;
.source "AbstractSessionManager.java"

# interfaces
.implements Ljavax/servlet/SessionCookieConfig;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/eclipse/jetty/server/session/AbstractSessionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/eclipse/jetty/server/session/AbstractSessionManager;


# direct methods
.method constructor <init>(Lorg/eclipse/jetty/server/session/AbstractSessionManager;)V
    .locals 0

    .prologue
    .line 863
    iput-object p1, p0, Lorg/eclipse/jetty/server/session/AbstractSessionManager$2;->this$0:Lorg/eclipse/jetty/server/session/AbstractSessionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMaxAge()I
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lorg/eclipse/jetty/server/session/AbstractSessionManager$2;->this$0:Lorg/eclipse/jetty/server/session/AbstractSessionManager;

    iget v0, v0, Lorg/eclipse/jetty/server/session/AbstractSessionManager;->_maxCookieAge:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 885
    iget-object v0, p0, Lorg/eclipse/jetty/server/session/AbstractSessionManager$2;->this$0:Lorg/eclipse/jetty/server/session/AbstractSessionManager;

    iget-object v0, v0, Lorg/eclipse/jetty/server/session/AbstractSessionManager;->_sessionCookie:Ljava/lang/String;

    return-object v0
.end method

.method public isHttpOnly()Z
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lorg/eclipse/jetty/server/session/AbstractSessionManager$2;->this$0:Lorg/eclipse/jetty/server/session/AbstractSessionManager;

    iget-boolean v0, v0, Lorg/eclipse/jetty/server/session/AbstractSessionManager;->_httpOnly:Z

    return v0
.end method

.method public isSecure()Z
    .locals 1

    .prologue
    .line 903
    iget-object v0, p0, Lorg/eclipse/jetty/server/session/AbstractSessionManager$2;->this$0:Lorg/eclipse/jetty/server/session/AbstractSessionManager;

    iget-boolean v0, v0, Lorg/eclipse/jetty/server/session/AbstractSessionManager;->_secureCookies:Z

    return v0
.end method
