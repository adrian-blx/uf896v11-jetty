.class public interface abstract Lorg/eclipse/jetty/server/SessionManager;
.super Ljava/lang/Object;
.source "SessionManager.java"

# interfaces
.implements Lorg/eclipse/jetty/util/component/LifeCycle;


# static fields
.field public static final __DefaultSessionDomain:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    sput-object v0, Lorg/eclipse/jetty/server/SessionManager;->__DefaultSessionDomain:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract access(Ljavax/servlet/http/HttpSession;Z)Lorg/eclipse/jetty/http/HttpCookie;
.end method

.method public abstract complete(Ljavax/servlet/http/HttpSession;)V
.end method

.method public abstract getHttpSession(Ljava/lang/String;)Ljavax/servlet/http/HttpSession;
.end method

.method public abstract getNodeId(Ljavax/servlet/http/HttpSession;)Ljava/lang/String;
.end method

.method public abstract getSessionCookie(Ljavax/servlet/http/HttpSession;Ljava/lang/String;Z)Lorg/eclipse/jetty/http/HttpCookie;
.end method

.method public abstract getSessionCookieConfig()Ljavax/servlet/SessionCookieConfig;
.end method

.method public abstract getSessionIdPathParameterNamePrefix()Ljava/lang/String;
.end method

.method public abstract isCheckingRemoteSessionIdEncoding()Z
.end method

.method public abstract isUsingCookies()Z
.end method

.method public abstract isUsingURLs()Z
.end method

.method public abstract isValid(Ljavax/servlet/http/HttpSession;)Z
.end method

.method public abstract newHttpSession(Ljavax/servlet/http/HttpServletRequest;)Ljavax/servlet/http/HttpSession;
.end method

.method public abstract setSessionHandler(Lorg/eclipse/jetty/server/session/SessionHandler;)V
.end method
