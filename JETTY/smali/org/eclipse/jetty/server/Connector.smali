.class public interface abstract Lorg/eclipse/jetty/server/Connector;
.super Ljava/lang/Object;
.source "Connector.java"

# interfaces
.implements Lorg/eclipse/jetty/util/component/LifeCycle;


# virtual methods
.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract customize(Lorg/eclipse/jetty/io/EndPoint;Lorg/eclipse/jetty/server/Request;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getConfidentialPort()I
.end method

.method public abstract getConfidentialScheme()Ljava/lang/String;
.end method

.method public abstract getConnection()Ljava/lang/Object;
.end method

.method public abstract getIntegralPort()I
.end method

.method public abstract getIntegralScheme()Ljava/lang/String;
.end method

.method public abstract getLocalPort()I
.end method

.method public abstract getLowResourceMaxIdleTime()I
.end method

.method public abstract getMaxIdleTime()I
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getResolveNames()Z
.end method

.method public abstract getServer()Lorg/eclipse/jetty/server/Server;
.end method

.method public abstract isConfidential(Lorg/eclipse/jetty/server/Request;)Z
.end method

.method public abstract isIntegral(Lorg/eclipse/jetty/server/Request;)Z
.end method

.method public abstract isLowResources()Z
.end method

.method public abstract open()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract persist(Lorg/eclipse/jetty/io/EndPoint;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setPort(I)V
.end method

.method public abstract setServer(Lorg/eclipse/jetty/server/Server;)V
.end method
