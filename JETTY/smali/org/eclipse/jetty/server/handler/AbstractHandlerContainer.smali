.class public abstract Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;
.super Lorg/eclipse/jetty/server/handler/AbstractHandler;
.source "AbstractHandlerContainer.java"

# interfaces
.implements Lorg/eclipse/jetty/server/HandlerContainer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/eclipse/jetty/server/handler/AbstractHandler;-><init>()V

    .line 40
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/Appendable;Ljava/lang/String;)V
    .locals 3
    .param p1, "out"    # Ljava/lang/Appendable;
    .param p2, "indent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;->dumpThis(Ljava/lang/Appendable;)V

    .line 121
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/Collection;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;->getBeans()Ljava/util/Collection;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;->getHandlers()[Lorg/eclipse/jetty/server/Handler;

    move-result-object v2

    invoke-static {v2}, Lorg/eclipse/jetty/util/TypeUtil;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p1, p2, v0}, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;->dump(Ljava/lang/Appendable;Ljava/lang/String;[Ljava/util/Collection;)V

    .line 122
    return-void
.end method

.method protected expandChildren(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 0
    .param p1, "list"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 69
    .local p2, "byClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    return-object p1
.end method

.method protected expandHandler(Lorg/eclipse/jetty/server/Handler;Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .param p1, "handler"    # Lorg/eclipse/jetty/server/Handler;
    .param p2, "list"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/eclipse/jetty/server/Handler;",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<",
            "Lorg/eclipse/jetty/server/Handler;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 75
    .local p3, "byClass":Ljava/lang/Class;, "Ljava/lang/Class<Lorg/eclipse/jetty/server/Handler;>;"
    if-nez p1, :cond_0

    move-object v2, p2

    .line 90
    .end local p1    # "handler":Lorg/eclipse/jetty/server/Handler;
    .end local p2    # "list":Ljava/lang/Object;
    .local v2, "list":Ljava/lang/Object;
    :goto_0
    return-object v2

    .line 78
    .end local v2    # "list":Ljava/lang/Object;
    .restart local p1    # "handler":Lorg/eclipse/jetty/server/Handler;
    .restart local p2    # "list":Ljava/lang/Object;
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    :cond_1
    invoke-static {p2, p1}, Lorg/eclipse/jetty/util/LazyList;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 81
    :cond_2
    instance-of v3, p1, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;

    if-eqz v3, :cond_4

    .line 82
    check-cast p1, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;

    .end local p1    # "handler":Lorg/eclipse/jetty/server/Handler;
    invoke-virtual {p1, p2, p3}, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;->expandChildren(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    :cond_3
    :goto_1
    move-object v2, p2

    .line 90
    .end local p2    # "list":Ljava/lang/Object;
    .restart local v2    # "list":Ljava/lang/Object;
    goto :goto_0

    .line 83
    .end local v2    # "list":Ljava/lang/Object;
    .restart local p1    # "handler":Lorg/eclipse/jetty/server/Handler;
    .restart local p2    # "list":Ljava/lang/Object;
    :cond_4
    instance-of v3, p1, Lorg/eclipse/jetty/server/HandlerContainer;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 85
    check-cast v0, Lorg/eclipse/jetty/server/HandlerContainer;

    .line 86
    .local v0, "container":Lorg/eclipse/jetty/server/HandlerContainer;
    if-nez p3, :cond_5

    invoke-interface {v0}, Lorg/eclipse/jetty/server/HandlerContainer;->getChildHandlers()[Lorg/eclipse/jetty/server/Handler;

    move-result-object v1

    .line 87
    .local v1, "handlers":[Lorg/eclipse/jetty/server/Handler;
    :goto_2
    invoke-static {p2, v1}, Lorg/eclipse/jetty/util/LazyList;->addArray(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto :goto_1

    .line 86
    .end local v1    # "handlers":[Lorg/eclipse/jetty/server/Handler;
    :cond_5
    invoke-interface {v0, p3}, Lorg/eclipse/jetty/server/HandlerContainer;->getChildHandlersByClass(Ljava/lang/Class;)[Lorg/eclipse/jetty/server/Handler;

    move-result-object v1

    goto :goto_2
.end method

.method public getChildHandlerByClass(Ljava/lang/Class;)Lorg/eclipse/jetty/server/Handler;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lorg/eclipse/jetty/server/Handler;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .local p1, "byclass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v1, 0x0

    .line 60
    invoke-virtual {p0, v1, p1}, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;->expandChildren(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 61
    .local v0, "list":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 63
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/eclipse/jetty/util/LazyList;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/eclipse/jetty/server/Handler;

    goto :goto_0
.end method

.method public getChildHandlers()[Lorg/eclipse/jetty/server/Handler;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-virtual {p0, v0, v0}, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;->expandChildren(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 46
    const-class v1, Lorg/eclipse/jetty/server/Handler;

    invoke-static {v0, v1}, Lorg/eclipse/jetty/util/LazyList;->toArray(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/eclipse/jetty/server/Handler;

    check-cast v0, [Lorg/eclipse/jetty/server/Handler;

    return-object v0
.end method

.method public getChildHandlersByClass(Ljava/lang/Class;)[Lorg/eclipse/jetty/server/Handler;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Lorg/eclipse/jetty/server/Handler;"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "byclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1}, Lorg/eclipse/jetty/server/handler/AbstractHandlerContainer;->expandChildren(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 53
    .local v0, "list":Ljava/lang/Object;
    invoke-static {v0, p1}, Lorg/eclipse/jetty/util/LazyList;->toArray(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/eclipse/jetty/server/Handler;

    check-cast v1, [Lorg/eclipse/jetty/server/Handler;

    return-object v1
.end method
