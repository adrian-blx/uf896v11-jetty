.class public Lcom/jetty2m/config/MifiConfiguration;
.super Ljava/lang/Object;
.source "MifiConfiguration.java"


# static fields
.field private static instance:Lcom/jetty2m/config/MifiConfiguration;


# instance fields
.field public DHCPrangeHigh:Ljava/lang/String;

.field public DHCPrangeLow:Ljava/lang/String;

.field public SimcardSlot:I

.field public WANCurrentIccid:Ljava/lang/String;

.field public WANCurrentImei:Ljava/lang/String;

.field public WANCurrentImsi:Ljava/lang/String;

.field public WANautoConnect:Z

.field public WANdefaultAPN:I

.field public WANnetworkType:I

.field public WANtrafficStartTime:J

.field public WANtrafficTotalCount:J

.field public WIFIchannel:I

.field public WIFIencrypt:I

.field public WIFIhwmode:Ljava/lang/String;

.field public WIFImaxSta:I

.field public WIFIpassword:Ljava/lang/String;

.field public WIFIssid:Ljava/lang/String;

.field public WIFIssidhidden:Z

.field public loginPassword:Ljava/lang/String;

.field public mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    sput-object v0, Lcom/jetty2m/config/MifiConfiguration;->instance:Lcom/jetty2m/config/MifiConfiguration;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string v0, "admin"

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->loginPassword:Ljava/lang/String;

    .line 38
    iput-boolean v1, p0, Lcom/jetty2m/config/MifiConfiguration;->WANautoConnect:Z

    .line 39
    iput v2, p0, Lcom/jetty2m/config/MifiConfiguration;->WANnetworkType:I

    .line 40
    iput v1, p0, Lcom/jetty2m/config/MifiConfiguration;->SimcardSlot:I

    .line 41
    const-string v0, "4G UFI-0000"

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    .line 42
    iput-boolean v2, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssidhidden:Z

    .line 43
    const-string v0, "n"

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIhwmode:Ljava/lang/String;

    .line 44
    const/4 v0, 0x6

    iput v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIchannel:I

    .line 45
    const/16 v0, 0xa

    iput v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFImaxSta:I

    .line 46
    iput v1, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIencrypt:I

    .line 47
    const-string v0, "1234567890"

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIpassword:Ljava/lang/String;

    .line 48
    const-string v0, "192.168.43.2"

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->DHCPrangeLow:Ljava/lang/String;

    .line 49
    const-string v0, "192.168.43.254"

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->DHCPrangeHigh:Ljava/lang/String;

    .line 51
    iput v2, p0, Lcom/jetty2m/config/MifiConfiguration;->WANdefaultAPN:I

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImei:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImsi:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentIccid:Ljava/lang/String;

    .line 56
    iput-wide v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANtrafficStartTime:J

    .line 57
    iput-wide v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANtrafficTotalCount:J

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->mContext:Landroid/content/Context;

    .line 73
    iput-object p1, p0, Lcom/jetty2m/config/MifiConfiguration;->mContext:Landroid/content/Context;

    .line 76
    invoke-virtual {p0}, Lcom/jetty2m/config/MifiConfiguration;->loadFromFile()V

    .line 77
    invoke-virtual {p0}, Lcom/jetty2m/config/MifiConfiguration;->checkssidimei()Z

    move-result v0

    if-ne v1, v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/jetty2m/config/MifiConfiguration;->saveToFile()V

    .line 81
    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/misc/wifi/hostapd.conf"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 82
    return-void
.end method

.method public static getInstance()Lcom/jetty2m/config/MifiConfiguration;
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/jetty2m/config/MifiConfiguration;->instance:Lcom/jetty2m/config/MifiConfiguration;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/jetty2m/config/MifiConfiguration;

    invoke-static {}, Lcom/jetty2m/service/MifiService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jetty2m/config/MifiConfiguration;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/jetty2m/config/MifiConfiguration;->instance:Lcom/jetty2m/config/MifiConfiguration;

    .line 68
    :cond_0
    sget-object v0, Lcom/jetty2m/config/MifiConfiguration;->instance:Lcom/jetty2m/config/MifiConfiguration;

    return-object v0
.end method


# virtual methods
.method public checkssidimei()Z
    .locals 10

    .prologue
    const/4 v8, 0x4

    .line 177
    const-string v0, "4G UFI-0000"

    .line 178
    .local v0, "dssid":Ljava/lang/String;
    const-string v6, "4G UFI-0000"

    .line 179
    .local v6, "ossid":Ljava/lang/String;
    iget-object v2, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    .line 180
    .local v2, "jssid":Ljava/lang/String;
    iget-object v1, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImei:Ljava/lang/String;

    .line 181
    .local v1, "jimei":Ljava/lang/String;
    const/4 v3, 0x0

    .line 184
    .local v3, "needupdate":Z
    invoke-static {}, Lcom/jetty2m/device/WanDataController;->getInstance()Lcom/jetty2m/device/WanDataController;

    move-result-object v7

    invoke-virtual {v7}, Lcom/jetty2m/device/WanDataController;->getIMEI()Ljava/lang/String;

    move-result-object v4

    .line 186
    .local v4, "nimei":Ljava/lang/String;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v8, :cond_3

    .line 187
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 189
    const/4 v7, 0x0

    .line 205
    :goto_0
    return v7

    .line 191
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v8, :cond_1

    .line 192
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "4G UFI-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x4

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 195
    :cond_1
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 196
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "4G UFI-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x4

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 197
    .local v5, "nssid":Ljava/lang/String;
    iput-object v5, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    .line 198
    const-string v7, "MifiConfiguration"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "checkssidimei ssid update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    .end local v5    # "nssid":Ljava/lang/String;
    :cond_2
    iput-object v4, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImei:Ljava/lang/String;

    .line 201
    const/4 v3, 0x1

    :cond_3
    move v7, v3

    .line 205
    goto :goto_0
.end method

.method public deleteFileSd()V
    .locals 2

    .prologue
    .line 363
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/mificonfig.json"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 365
    const-string v0, "MifiConfiguration"

    const-string v1, "delete file ok "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    return-void
.end method

.method public getSimCardSlot()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/jetty2m/config/MifiConfiguration;->SimcardSlot:I

    return v0
.end method

.method public getWifiDefaultSsid()Ljava/lang/String;
    .locals 5

    .prologue
    .line 148
    const-string v1, "4G UFI-0000"

    .line 163
    .local v1, "ssid":Ljava/lang/String;
    invoke-static {}, Lcom/jetty2m/device/WanDataController;->getInstance()Lcom/jetty2m/device/WanDataController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jetty2m/device/WanDataController;->getIMEI()Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "imei":Ljava/lang/String;
    const-string v2, "MifiConfiguration"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getWifiDefaultSsid imei "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x4

    if-le v2, v3, :cond_0

    .line 166
    iput-object v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImei:Ljava/lang/String;

    .line 167
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "4G UFI-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 173
    :goto_0
    return-object v1

    .line 169
    :cond_0
    const-string v1, "4G UFI-0000"

    goto :goto_0
.end method

.method public getnetworktype()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/jetty2m/config/MifiConfiguration;->WANnetworkType:I

    return v0
.end method

.method public loadFromFile()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 88
    :try_start_0
    const-string v3, "/data/mificonfig.json"

    invoke-virtual {p0, v3}, Lcom/jetty2m/config/MifiConfiguration;->readFileSd(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "acceptjson":Ljava/lang/String;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 92
    .local v2, "jo":Lorg/json/JSONObject;
    const-string v3, "loginPassword"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->loginPassword:Ljava/lang/String;

    .line 93
    const-string v3, "WANautoConnect"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANautoConnect:Z

    .line 94
    const-string v3, "WANnetworkType"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANnetworkType:I

    .line 95
    const-string v3, "SimcardSlot"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/jetty2m/config/MifiConfiguration;->SimcardSlot:I

    .line 97
    const-string v3, "WIFIssid"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    .line 98
    const-string v3, "WIFIssidhidden"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssidhidden:Z

    .line 99
    const-string v3, "WIFIhwmode"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIhwmode:Ljava/lang/String;

    .line 100
    const-string v3, "WIFIchannel"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIchannel:I

    .line 101
    const-string v3, "WIFImaxSta"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFImaxSta:I

    .line 102
    const-string v3, "WIFIencrypt"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIencrypt:I

    .line 103
    const-string v3, "WIFIpassword"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIpassword:Ljava/lang/String;

    .line 105
    const-string v3, "DHCPrangeLow"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->DHCPrangeLow:Ljava/lang/String;

    .line 106
    const-string v3, "DHCPrangeHigh"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->DHCPrangeHigh:Ljava/lang/String;

    .line 108
    const-string v3, "WANdefaultAPN"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANdefaultAPN:I

    .line 109
    const-string v3, "WANCurrentImei"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImei:Ljava/lang/String;

    .line 110
    const-string v3, "WANCurrentImsi"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImsi:Ljava/lang/String;

    .line 111
    const-string v3, "WANCurrentIccid"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentIccid:Ljava/lang/String;

    .line 112
    const-string v3, "WANtrafficStartTime"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANtrafficStartTime:J

    .line 113
    const-string v3, "WANtrafficTotalCount"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANtrafficTotalCount:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    .end local v0    # "acceptjson":Ljava/lang/String;
    .end local v2    # "jo":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 115
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "MifiConfiguration"

    const-string v4, "read error"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const-string v3, "admin"

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->loginPassword:Ljava/lang/String;

    .line 119
    iput-boolean v6, p0, Lcom/jetty2m/config/MifiConfiguration;->WANautoConnect:Z

    .line 120
    iput v5, p0, Lcom/jetty2m/config/MifiConfiguration;->WANnetworkType:I

    .line 121
    iput v6, p0, Lcom/jetty2m/config/MifiConfiguration;->SimcardSlot:I

    .line 123
    invoke-virtual {p0}, Lcom/jetty2m/config/MifiConfiguration;->getWifiDefaultSsid()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    .line 124
    iput-boolean v5, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssidhidden:Z

    .line 125
    const-string v3, "n"

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIhwmode:Ljava/lang/String;

    .line 126
    const/4 v3, 0x6

    iput v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIchannel:I

    .line 127
    const/16 v3, 0xa

    iput v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFImaxSta:I

    .line 128
    iput v7, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIencrypt:I

    .line 129
    const-string v3, "1234567890"

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIpassword:Ljava/lang/String;

    .line 131
    const-string v3, "192.168.43.2"

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->DHCPrangeLow:Ljava/lang/String;

    .line 132
    const-string v3, "192.168.43.254"

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->DHCPrangeHigh:Ljava/lang/String;

    .line 134
    iput v5, p0, Lcom/jetty2m/config/MifiConfiguration;->WANdefaultAPN:I

    .line 135
    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImei:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImei:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v7, :cond_0

    .line 139
    :goto_1
    const-string v3, ""

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImsi:Ljava/lang/String;

    .line 140
    const-string v3, ""

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentIccid:Ljava/lang/String;

    .line 141
    iput-wide v8, p0, Lcom/jetty2m/config/MifiConfiguration;->WANtrafficStartTime:J

    .line 142
    iput-wide v8, p0, Lcom/jetty2m/config/MifiConfiguration;->WANtrafficTotalCount:J

    .line 143
    invoke-virtual {p0}, Lcom/jetty2m/config/MifiConfiguration;->saveToFile()V

    goto :goto_0

    .line 137
    :cond_0
    const-string v3, ""

    iput-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImei:Ljava/lang/String;

    goto :goto_1
.end method

.method public readFileSd(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    const-string v5, ""

    .line 317
    .local v5, "res":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 318
    .local v2, "file":Ljava/io/File;
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 320
    .local v3, "fin":Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 321
    .local v4, "length":I
    new-array v0, v4, [B

    .line 322
    .local v0, "buffer":[B
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    .line 323
    const-string v6, "UTF-8"

    invoke-static {v0, v6}, Lorg/apache/http/util/EncodingUtils;->getString([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 324
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    .end local v0    # "buffer":[B
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fin":Ljava/io/InputStream;
    .end local v4    # "length":I
    :goto_0
    return-object v5

    .line 326
    :catch_0
    move-exception v1

    .line 327
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/jetty2m/config/MifiConfiguration;->deleteFileSd()V

    .line 260
    return-void
.end method

.method public saveFileSd(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 335
    const/4 v0, 0x0

    .line 336
    .local v0, "bw":Ljava/io/BufferedWriter;
    const/4 v5, 0x0

    .line 339
    .local v5, "isSaveSuccess":Z
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 340
    .local v3, "file":Ljava/io/File;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 342
    .local v4, "fileOutputStream":Ljava/io/FileOutputStream;
    new-instance v6, Ljava/io/OutputStreamWriter;

    invoke-direct {v6, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 343
    .local v6, "outputStreamWriter":Ljava/io/OutputStreamWriter;
    new-instance v1, Ljava/io/BufferedWriter;

    invoke-direct {v1, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .local v1, "bw":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v1, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 345
    const/4 v5, 0x1

    .line 351
    if-eqz v1, :cond_2

    .line 353
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 359
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v6    # "outputStreamWriter":Ljava/io/OutputStreamWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    :cond_0
    :goto_0
    return v5

    .line 354
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "outputStreamWriter":Ljava/io/OutputStreamWriter;
    :catch_0
    move-exception v2

    .line 355
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 356
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_0

    .line 346
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v6    # "outputStreamWriter":Ljava/io/OutputStreamWriter;
    :catch_1
    move-exception v2

    .line 347
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 351
    if-eqz v0, :cond_0

    .line 353
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 354
    :catch_2
    move-exception v2

    .line 355
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 348
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 349
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 351
    if-eqz v0, :cond_0

    .line 353
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 354
    :catch_4
    move-exception v2

    .line 355
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 351
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    if-eqz v0, :cond_1

    .line 353
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 356
    :cond_1
    :goto_4
    throw v7

    .line 354
    :catch_5
    move-exception v2

    .line 355
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 351
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "outputStreamWriter":Ljava/io/OutputStreamWriter;
    :catchall_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_3

    .line 348
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_2

    .line 346
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catch_7
    move-exception v2

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_1

    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :cond_2
    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_0
.end method

.method public saveToFile()V
    .locals 5

    .prologue
    .line 221
    const-string v2, "MifiConfiguration"

    const-string v3, "saveToFile"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 225
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v2, "loginPassword"

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->loginPassword:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 226
    const-string v2, "WANautoConnect"

    iget-boolean v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANautoConnect:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 227
    const-string v2, "WANnetworkType"

    iget v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANnetworkType:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 228
    const-string v2, "SimcardSlot"

    iget v3, p0, Lcom/jetty2m/config/MifiConfiguration;->SimcardSlot:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 230
    const-string v2, "WIFIssid"

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 231
    const-string v2, "WIFIssidhidden"

    iget-boolean v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIssidhidden:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 232
    const-string v2, "WIFIhwmode"

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIhwmode:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 233
    const-string v2, "WIFIchannel"

    iget v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIchannel:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 234
    const-string v2, "WIFImaxSta"

    iget v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFImaxSta:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 235
    const-string v2, "WIFIencrypt"

    iget v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIencrypt:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 236
    const-string v2, "WIFIpassword"

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WIFIpassword:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 238
    const-string v2, "DHCPrangeLow"

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->DHCPrangeLow:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 239
    const-string v2, "DHCPrangeHigh"

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->DHCPrangeHigh:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 241
    const-string v2, "WANdefaultAPN"

    iget v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANdefaultAPN:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 242
    const-string v2, "WANCurrentImei"

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImei:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 243
    const-string v2, "WANCurrentImsi"

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentImsi:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 244
    const-string v2, "WANCurrentIccid"

    iget-object v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANCurrentIccid:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 245
    const-string v2, "WANtrafficStartTime"

    iget-wide v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANtrafficStartTime:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 246
    const-string v2, "WANtrafficTotalCount"

    iget-wide v3, p0, Lcom/jetty2m/config/MifiConfiguration;->WANtrafficTotalCount:J

    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 249
    const-string v2, "/data/mificonfig.json"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/jetty2m/config/MifiConfiguration;->saveFileSd(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 250
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "MifiConfiguration"

    const-string v3, "save error"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
