.class public Lcom/jetty2m/server/AjaxSevlet;
.super Ljavax/servlet/http/HttpServlet;
.source "AjaxSevlet.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private context:Landroid/content/Context;

.field private mDeviceController:Lcom/jetty2m/device/DeviceController;

.field private mGpsController:Lcom/jetty2m/device/GpsController;

.field private mWanApnController:Lcom/jetty2m/device/WanApnController;

.field private mWanDataController:Lcom/jetty2m/device/WanDataController;

.field private mWifiApController:Lcom/jetty2m/device/WifiApController;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljavax/servlet/http/HttpServlet;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    .line 38
    iput-object v0, p0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    .line 39
    iput-object v0, p0, Lcom/jetty2m/server/AjaxSevlet;->mWanApnController:Lcom/jetty2m/device/WanApnController;

    .line 40
    iput-object v0, p0, Lcom/jetty2m/server/AjaxSevlet;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    .line 41
    iput-object v0, p0, Lcom/jetty2m/server/AjaxSevlet;->mGpsController:Lcom/jetty2m/device/GpsController;

    .line 42
    iput-object v0, p0, Lcom/jetty2m/server/AjaxSevlet;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected doGet(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;)V
    .locals 7
    .param p1, "req"    # Ljavax/servlet/http/HttpServletRequest;
    .param p2, "resp"    # Ljavax/servlet/http/HttpServletResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/servlet/ServletException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    const-string v4, "UTF-8"

    invoke-interface {p2, v4}, Ljavax/servlet/http/HttpServletResponse;->setCharacterEncoding(Ljava/lang/String;)V

    .line 50
    const-string v4, "text/json"

    invoke-interface {p2, v4}, Ljavax/servlet/http/HttpServletResponse;->setContentType(Ljava/lang/String;)V

    .line 52
    iget-object v4, p0, Lcom/jetty2m/server/AjaxSevlet;->context:Landroid/content/Context;

    if-nez v4, :cond_0

    .line 53
    invoke-static {}, Lcom/jetty2m/service/MifiService;->getContext()Landroid/content/Context;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 54
    invoke-static {}, Lcom/jetty2m/device/WifiApController;->getInstance()Lcom/jetty2m/device/WifiApController;

    move-result-object v4

    iput-object v4, p0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    .line 55
    invoke-static {}, Lcom/jetty2m/device/WanDataController;->getInstance()Lcom/jetty2m/device/WanDataController;

    move-result-object v4

    iput-object v4, p0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    .line 56
    invoke-static {}, Lcom/jetty2m/device/WanApnController;->getInstance()Lcom/jetty2m/device/WanApnController;

    move-result-object v4

    iput-object v4, p0, Lcom/jetty2m/server/AjaxSevlet;->mWanApnController:Lcom/jetty2m/device/WanApnController;

    .line 57
    invoke-static {}, Lcom/jetty2m/device/DeviceController;->getInstance()Lcom/jetty2m/device/DeviceController;

    move-result-object v4

    iput-object v4, p0, Lcom/jetty2m/server/AjaxSevlet;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    .line 58
    invoke-static {}, Lcom/jetty2m/device/GpsController;->getInstance()Lcom/jetty2m/device/GpsController;

    move-result-object v4

    iput-object v4, p0, Lcom/jetty2m/server/AjaxSevlet;->mGpsController:Lcom/jetty2m/device/GpsController;

    .line 60
    invoke-static {}, Lcom/jetty2m/service/MifiService;->getContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/jetty2m/server/AjaxSevlet;->context:Landroid/content/Context;

    .line 64
    :cond_0
    const-string v0, ""

    .line 65
    .local v0, "acceptjson":Ljava/lang/String;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-interface {p1}, Ljavax/servlet/http/HttpServletRequest;->getInputStream()Ljavax/servlet/ServletInputStream;

    move-result-object v5

    const-string v6, "utf-8"

    invoke-direct {v4, v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 68
    .local v1, "br":Ljava/io/BufferedReader;
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v4, ""

    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 70
    .local v2, "sb":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, "temp":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 74
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v4, "Ajax"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "request: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    return-void
.end method

.method protected doPost(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;)V
    .locals 52
    .param p1, "req"    # Ljavax/servlet/http/HttpServletRequest;
    .param p2, "resp"    # Ljavax/servlet/http/HttpServletResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/servlet/ServletException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    const-string v3, "UTF-8"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Ljavax/servlet/http/HttpServletResponse;->setCharacterEncoding(Ljava/lang/String;)V

    .line 92
    const-string v3, "text/json"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Ljavax/servlet/http/HttpServletResponse;->setContentType(Ljava/lang/String;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->context:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 95
    invoke-static {}, Lcom/jetty2m/service/MifiService;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 96
    invoke-static {}, Lcom/jetty2m/device/WifiApController;->getInstance()Lcom/jetty2m/device/WifiApController;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    .line 97
    invoke-static {}, Lcom/jetty2m/device/WanDataController;->getInstance()Lcom/jetty2m/device/WanDataController;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    .line 98
    invoke-static {}, Lcom/jetty2m/device/WanApnController;->getInstance()Lcom/jetty2m/device/WanApnController;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanApnController:Lcom/jetty2m/device/WanApnController;

    .line 99
    invoke-static {}, Lcom/jetty2m/device/DeviceController;->getInstance()Lcom/jetty2m/device/DeviceController;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    .line 100
    invoke-static {}, Lcom/jetty2m/device/GpsController;->getInstance()Lcom/jetty2m/device/GpsController;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mGpsController:Lcom/jetty2m/device/GpsController;

    .line 102
    invoke-static {}, Lcom/jetty2m/service/MifiService;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->context:Landroid/content/Context;

    .line 106
    :cond_0
    const-string v10, ""

    .line 107
    .local v10, "acceptjson":Ljava/lang/String;
    new-instance v14, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-interface/range {p1 .. p1}, Ljavax/servlet/http/HttpServletRequest;->getInputStream()Ljavax/servlet/ServletInputStream;

    move-result-object v48

    const-string v49, "utf-8"

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    invoke-direct {v3, v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v14, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 110
    .local v14, "br":Ljava/io/BufferedReader;
    new-instance v42, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v42

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 112
    .local v42, "sb":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v14}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v46

    .local v46, "temp":Ljava/lang/String;
    if-eqz v46, :cond_1

    .line 113
    move-object/from16 v0, v42

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {v14}, Ljava/io/BufferedReader;->close()V

    .line 116
    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    .line 118
    const-string v3, "Ajax"

    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    const-string v49, "request: "

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :try_start_0
    new-instance v28, Lorg/json/JSONObject;

    move-object/from16 v0, v28

    invoke-direct {v0, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 123
    .local v28, "jo":Lorg/json/JSONObject;
    const-string v3, "funcNo"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v25

    .line 126
    .local v25, "funcNo":I
    const/16 v3, 0x3e8

    move/from16 v0, v25

    if-ne v0, v3, :cond_5

    .line 127
    const-string v3, "username"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    .line 128
    .local v47, "username":Ljava/lang/String;
    const-string v3, "password"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 130
    .local v37, "password":Ljava/lang/String;
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 131
    .local v30, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "admin"

    move-object/from16 v0, v47

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    iget-object v3, v3, Lcom/jetty2m/config/MifiConfiguration;->loginPassword:Ljava/lang/String;

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 133
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 134
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 135
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 137
    .local v41, "resultObject":Lorg/json/JSONObject;
    const-string v3, "imei"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getIMEI()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 138
    const-string v48, "conn_mode"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WanDataController;->getMobileDataEnable()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "0"

    :goto_1
    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 139
    const-string v3, "net_mode"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getPreferedNetworkType()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 140
    const-string v3, "sim_slot"

    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/config/MifiConfiguration;->getSimCardSlot()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 141
    const-string v3, "fwversion"

    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Lcom/jetty2m/device/DeviceController;->getDeviceName()Ljava/lang/String;

    move-result-object v49

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    const-string v49, " "

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    move-object/from16 v49, v0

    invoke-virtual/range {v49 .. v49}, Lcom/jetty2m/device/DeviceController;->getSWVersion()Ljava/lang/String;

    move-result-object v49

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 143
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 144
    .local v29, "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 145
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 151
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :goto_2
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 622
    .end local v25    # "funcNo":I
    .end local v28    # "jo":Lorg/json/JSONObject;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v37    # "password":Ljava/lang/String;
    .end local v47    # "username":Ljava/lang/String;
    :cond_2
    :goto_3
    return-void

    .line 138
    .restart local v25    # "funcNo":I
    .restart local v28    # "jo":Lorg/json/JSONObject;
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    .restart local v37    # "password":Ljava/lang/String;
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    .restart local v47    # "username":Ljava/lang/String;
    :cond_3
    const-string v3, "1"

    goto/16 :goto_1

    .line 148
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_4
    const-string v3, "flag"

    const-string v48, "0"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 149
    const-string v3, "error_info"

    const-string v48, "username or password error!"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 616
    .end local v25    # "funcNo":I
    .end local v28    # "jo":Lorg/json/JSONObject;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v37    # "password":Ljava/lang/String;
    .end local v47    # "username":Ljava/lang/String;
    :catch_0
    move-exception v22

    .line 617
    .local v22, "e":Ljava/lang/Exception;
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Exception;->printStackTrace()V

    .line 618
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    const-string v48, "flag:0"

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    goto :goto_3

    .line 153
    .end local v22    # "e":Ljava/lang/Exception;
    .restart local v25    # "funcNo":I
    .restart local v28    # "jo":Lorg/json/JSONObject;
    :cond_5
    const/16 v3, 0x3e9

    move/from16 v0, v25

    if-ne v0, v3, :cond_6

    .line 154
    :try_start_1
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 155
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 156
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 158
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 159
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    const-string v3, "rssi"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getSignalStrength()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 160
    const-string v3, "netmode"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getNetMode()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 161
    const-string v3, "netstatus"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getNetStatus()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 163
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 164
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 165
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 167
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 170
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_6
    const/16 v3, 0x3ea

    move/from16 v0, v25

    if-ne v0, v3, :cond_7

    .line 171
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 172
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 173
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 174
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 176
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    const-string v3, "IP"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getMobileIP()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 177
    const-string v3, "mask"

    const-string v48, "255.255.255.0"

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 178
    const-string v3, "dns"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWlanDNS()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 179
    const-string v3, "ssid"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getSsid()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 180
    const-string v3, "wlan_ip"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWlanIP()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 182
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 183
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 184
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 186
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 188
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_7
    const/16 v3, 0x3eb

    move/from16 v0, v25

    if-ne v0, v3, :cond_8

    .line 189
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 190
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 191
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 193
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 194
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    const-string v3, "up_bytes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getMobileTxBytes()J

    move-result-wide v48

    const-wide/16 v50, 0x400

    div-long v48, v48, v50

    move-object/from16 v0, v41

    move-wide/from16 v1, v48

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 195
    const-string v3, "down_bytes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getMobileRxBytes()J

    move-result-wide v48

    const-wide/16 v50, 0x400

    div-long v48, v48, v50

    move-object/from16 v0, v41

    move-wide/from16 v1, v48

    invoke-virtual {v0, v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 196
    const-string v3, "longtitude"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mGpsController:Lcom/jetty2m/device/GpsController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/GpsController;->getLongitude()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 197
    const-string v3, "latitude"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mGpsController:Lcom/jetty2m/device/GpsController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/GpsController;->getLatitude()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 198
    const-string v3, "client_num"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getClientNumber()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 199
    const-string v3, "maxSta"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWifiMaxSta()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 201
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 202
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 203
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 205
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 207
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_8
    const/16 v3, 0x3ec

    move/from16 v0, v25

    if-ne v0, v3, :cond_b

    .line 208
    const-string v3, "conn_mode"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 209
    .local v20, "conn_mode":Ljava/lang/String;
    const/4 v13, 0x0

    .line 211
    .local v13, "auto_conn":Z
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 212
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "1"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 213
    const/4 v13, 0x0

    .line 217
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    invoke-virtual {v3, v13}, Lcom/jetty2m/device/WanDataController;->setMobileDataEnable(Z)V

    .line 219
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    iput-boolean v13, v3, Lcom/jetty2m/config/MifiConfiguration;->WANautoConnect:Z

    .line 220
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jetty2m/config/MifiConfiguration;->saveToFile()V

    .line 222
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 223
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 224
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 214
    :cond_a
    const-string v3, "0"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 215
    const/4 v13, 0x1

    goto :goto_4

    .line 226
    .end local v13    # "auto_conn":Z
    .end local v20    # "conn_mode":Ljava/lang/String;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    :cond_b
    const/16 v3, 0x3ed

    move/from16 v0, v25

    if-ne v0, v3, :cond_c

    .line 228
    const-string v3, "net_mode"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v33

    .line 229
    .local v33, "net_mode":I
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 231
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move/from16 v0, v33

    invoke-virtual {v3, v0}, Lcom/jetty2m/device/WanDataController;->setPreferedNetworkType(I)V

    .line 232
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    move/from16 v0, v33

    iput v0, v3, Lcom/jetty2m/config/MifiConfiguration;->WANnetworkType:I

    .line 233
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jetty2m/config/MifiConfiguration;->saveToFile()V

    .line 235
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 236
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 237
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 239
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v33    # "net_mode":I
    :cond_c
    const/16 v3, 0x3ee

    move/from16 v0, v25

    if-ne v0, v3, :cond_f

    .line 240
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 241
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 242
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 244
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 245
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    const-string v48, "wifi_status"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->getWlanStatus()Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v3, "1"

    :goto_5
    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 246
    const-string v48, "ssid_flag"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->getWifiSsidHidden()Z

    move-result v3

    if-eqz v3, :cond_e

    const-string v3, "0"

    :goto_6
    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 247
    const-string v3, "mode"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWifiHwMode()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 249
    const-string v3, "ip"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWlanIP()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 250
    const-string v3, "mac"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWifiMAC()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 251
    const-string v3, "ssid"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getSsid()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 252
    const-string v3, "client_num"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getClientNumber()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 253
    const-string v3, "maxSta"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWifiMaxSta()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 256
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 257
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 258
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 260
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 245
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    :cond_d
    const-string v3, "0"

    goto/16 :goto_5

    .line 246
    :cond_e
    const-string v3, "1"

    goto/16 :goto_6

    .line 262
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_f
    const/16 v3, 0x3ef

    move/from16 v0, v25

    if-ne v0, v3, :cond_11

    .line 263
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 267
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "ssid_hidden"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Ljava/lang/String;

    .line 268
    .local v45, "ssidhidden":Ljava/lang/String;
    const-string v3, "mode"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v32

    .line 269
    .local v32, "mode":I
    const-string v3, "ssid"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Ljava/lang/String;

    .line 270
    .local v44, "ssid":Ljava/lang/String;
    const-string v3, "maxSta"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v31

    .line 272
    .local v31, "maxSta":I
    const/4 v3, 0x6

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v26, v0

    const/4 v3, 0x0

    const-string v48, "b"

    aput-object v48, v26, v3

    const/4 v3, 0x1

    const-string v48, "g-only"

    aput-object v48, v26, v3

    const/4 v3, 0x2

    const-string v48, "n-only"

    aput-object v48, v26, v3

    const/4 v3, 0x3

    const-string v48, "g"

    aput-object v48, v26, v3

    const/4 v3, 0x4

    const-string v48, "n"

    aput-object v48, v26, v3

    const/4 v3, 0x5

    const-string v48, "a"

    aput-object v48, v26, v3

    .line 276
    .local v26, "hw_mode":[Ljava/lang/String;
    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v48, 0x2

    move/from16 v0, v48

    if-ge v3, v0, :cond_10

    .line 277
    const-string v3, "flag"

    const-string v48, "0"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 278
    const-string v3, "error_info"

    const-string v48, "ssid too short!"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 296
    :goto_7
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 280
    :cond_10
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 281
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 283
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    const-string v48, "0"

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    move/from16 v0, v48

    invoke-virtual {v3, v0}, Lcom/jetty2m/device/WifiApController;->setWifiSsidHidden(Z)V

    .line 284
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    add-int/lit8 v48, v32, -0x1

    aget-object v48, v26, v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Lcom/jetty2m/device/WifiApController;->setWifiHwMode(Ljava/lang/String;)V

    .line 285
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v0, v44

    invoke-virtual {v3, v0}, Lcom/jetty2m/device/WifiApController;->setSsid(Ljava/lang/String;)V

    .line 286
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move/from16 v0, v31

    invoke-virtual {v3, v0}, Lcom/jetty2m/device/WifiApController;->setWifiMaxSta(I)V

    .line 288
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    const-string v48, "0"

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    move/from16 v0, v48

    iput-boolean v0, v3, Lcom/jetty2m/config/MifiConfiguration;->WIFIssidhidden:Z

    .line 289
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    add-int/lit8 v48, v32, -0x1

    aget-object v48, v26, v48

    move-object/from16 v0, v48

    iput-object v0, v3, Lcom/jetty2m/config/MifiConfiguration;->WIFIhwmode:Ljava/lang/String;

    .line 290
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    move-object/from16 v0, v44

    iput-object v0, v3, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    .line 291
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    move/from16 v0, v31

    iput v0, v3, Lcom/jetty2m/config/MifiConfiguration;->WIFImaxSta:I

    .line 292
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jetty2m/config/MifiConfiguration;->saveToFile()V

    .line 293
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->restartAP()V

    goto/16 :goto_7

    .line 298
    .end local v26    # "hw_mode":[Ljava/lang/String;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v31    # "maxSta":I
    .end local v32    # "mode":I
    .end local v44    # "ssid":Ljava/lang/String;
    .end local v45    # "ssidhidden":Ljava/lang/String;
    :cond_11
    const/16 v3, 0x3f0

    move/from16 v0, v25

    if-ne v0, v3, :cond_12

    .line 299
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 300
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 301
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 303
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->restartAP()V

    goto/16 :goto_3

    .line 306
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    :cond_12
    const/16 v3, 0x3f1

    move/from16 v0, v25

    if-ne v0, v3, :cond_13

    .line 307
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 308
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 309
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 311
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 313
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    const-string v3, "encryp_type"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWifiEncryptType()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 314
    const-string v3, "pwd"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v48

    move-object/from16 v0, v48

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 316
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 317
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 318
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 320
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 322
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_13
    const/16 v3, 0x3f2

    move/from16 v0, v25

    if-ne v0, v3, :cond_14

    .line 323
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 324
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 325
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 327
    const-string v3, "encryp_type"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v24

    .line 328
    .local v24, "encryp_type":I
    const-string v3, "pwd"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 330
    .local v8, "pwd":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v19

    .line 331
    .local v19, "conf":Landroid/net/wifi/WifiConfiguration;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    move/from16 v1, v24

    invoke-virtual {v3, v0, v1, v8}, Lcom/jetty2m/device/WifiApController;->setWifiApConfig(Ljava/lang/String;ILjava/lang/String;)Z

    .line 333
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    iput-object v0, v3, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    .line 334
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    move/from16 v0, v24

    iput v0, v3, Lcom/jetty2m/config/MifiConfiguration;->WIFIencrypt:I

    .line 335
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    iput-object v8, v3, Lcom/jetty2m/config/MifiConfiguration;->WIFIpassword:Ljava/lang/String;

    .line 336
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jetty2m/config/MifiConfiguration;->saveToFile()V

    .line 337
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->restartAP()V

    .line 339
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 341
    .end local v8    # "pwd":Ljava/lang/String;
    .end local v19    # "conf":Landroid/net/wifi/WifiConfiguration;
    .end local v24    # "encryp_type":I
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    :cond_14
    const/16 v3, 0x3f3

    move/from16 v0, v25

    if-ne v0, v3, :cond_16

    .line 342
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 343
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 344
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 346
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 348
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    const-string v3, "ip"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWlanIP()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 349
    const-string v3, "dns"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWlanDNS()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 350
    const-string v3, "range_low"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWlanDHCP_Low()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 351
    const-string v3, "range_high"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WifiApController;->getWlanDHCP_High()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 353
    new-instance v16, Lorg/json/JSONArray;

    invoke-direct/range {v16 .. v16}, Lorg/json/JSONArray;-><init>()V

    .line 354
    .local v16, "clientArray":Lorg/json/JSONArray;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->getWifiClients()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_15

    .line 355
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->getWifiClients()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .local v27, "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_15

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/jetty2m/device/WifiApController$WifiClient;

    .line 357
    .local v15, "client":Lcom/jetty2m/device/WifiApController$WifiClient;
    new-instance v17, Lorg/json/JSONObject;

    invoke-direct/range {v17 .. v17}, Lorg/json/JSONObject;-><init>()V

    .line 358
    .local v17, "clientObject":Lorg/json/JSONObject;
    const-string v3, "name"

    iget-object v0, v15, Lcom/jetty2m/device/WifiApController$WifiClient;->name:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 359
    const-string v3, "ip"

    iget-object v0, v15, Lcom/jetty2m/device/WifiApController$WifiClient;->ip:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 360
    const-string v3, "mac"

    iget-object v0, v15, Lcom/jetty2m/device/WifiApController$WifiClient;->mac:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 361
    const-string v3, "media"

    iget-object v0, v15, Lcom/jetty2m/device/WifiApController$WifiClient;->media:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 362
    invoke-virtual/range {v16 .. v17}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_8

    .line 366
    .end local v15    # "client":Lcom/jetty2m/device/WifiApController$WifiClient;
    .end local v17    # "clientObject":Lorg/json/JSONObject;
    .end local v27    # "i$":Ljava/util/Iterator;
    :cond_15
    const-string v3, "device_arr"

    move-object/from16 v0, v41

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 368
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 369
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 370
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 372
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 374
    .end local v16    # "clientArray":Lorg/json/JSONArray;
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_16
    const/16 v3, 0x3f4

    move/from16 v0, v25

    if-ne v0, v3, :cond_17

    .line 375
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 376
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 377
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 379
    const-string v3, "dns"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 381
    .local v21, "dns":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/jetty2m/device/WifiApController;->setWlanDns(Ljava/lang/String;)V

    .line 383
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 385
    .end local v21    # "dns":Ljava/lang/String;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    :cond_17
    const/16 v3, 0x3f5

    move/from16 v0, v25

    if-ne v0, v3, :cond_18

    .line 387
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 389
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 390
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 392
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    invoke-virtual {v3}, Lcom/jetty2m/device/DeviceController;->rebootDevice()V

    .line 394
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 396
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    :cond_18
    const/16 v3, 0x3f6

    move/from16 v0, v25

    if-ne v0, v3, :cond_19

    .line 398
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 400
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 401
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 403
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    invoke-virtual {v3}, Lcom/jetty2m/device/DeviceController;->restoreDevice()V

    .line 405
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 407
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    :cond_19
    const/16 v3, 0x3f7

    move/from16 v0, v25

    if-ne v0, v3, :cond_1a

    .line 409
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 411
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 412
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 414
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 416
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    const-string v3, "sim_status"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getSIMStatus()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 417
    const-string v3, "imsi"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getIMSI()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 418
    const-string v3, "imei"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getIMEI()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 424
    const-string v3, "iccid"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getICCID()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 426
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 427
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 428
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 430
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 432
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_1a
    const/16 v3, 0x3f8

    move/from16 v0, v25

    if-ne v0, v3, :cond_1c

    .line 434
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 436
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 437
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 439
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 440
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    const-string v3, "profile_num"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanApnController:Lcom/jetty2m/device/WanApnController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanApnController;->getCurrentAPN()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 442
    new-instance v12, Lorg/json/JSONArray;

    invoke-direct {v12}, Lorg/json/JSONArray;-><init>()V

    .line 443
    .local v12, "apnsArray":Lorg/json/JSONArray;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanApnController:Lcom/jetty2m/device/WanApnController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WanApnController;->getAPNList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .restart local v27    # "i$":Ljava/util/Iterator;
    :goto_9
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/jetty2m/device/WanApnController$APN;

    .line 444
    .local v6, "apn":Lcom/jetty2m/device/WanApnController$APN;
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 446
    .local v11, "apnObject":Lorg/json/JSONObject;
    const-string v3, "no"

    iget-object v0, v6, Lcom/jetty2m/device/WanApnController$APN;->id:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v11, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 447
    const-string v3, "name"

    iget-object v0, v6, Lcom/jetty2m/device/WanApnController$APN;->name:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v11, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 448
    const-string v3, "apn"

    iget-object v0, v6, Lcom/jetty2m/device/WanApnController$APN;->apn:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v11, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 449
    const-string v3, "user"

    iget-object v0, v6, Lcom/jetty2m/device/WanApnController$APN;->user:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v11, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 450
    const-string v3, "pwd"

    iget-object v0, v6, Lcom/jetty2m/device/WanApnController$APN;->password:Ljava/lang/String;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-virtual {v11, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 451
    const-string v3, "auth"

    iget v0, v6, Lcom/jetty2m/device/WanApnController$APN;->authType:I

    move/from16 v48, v0

    move/from16 v0, v48

    invoke-virtual {v11, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 453
    invoke-virtual {v12, v11}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_9

    .line 455
    .end local v6    # "apn":Lcom/jetty2m/device/WanApnController$APN;
    .end local v11    # "apnObject":Lorg/json/JSONObject;
    :cond_1b
    const-string v3, "info_arr"

    move-object/from16 v0, v41

    invoke-virtual {v0, v3, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 457
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 458
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 459
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 461
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 463
    .end local v12    # "apnsArray":Lorg/json/JSONArray;
    .end local v27    # "i$":Ljava/util/Iterator;
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_1c
    const/16 v3, 0x3f9

    move/from16 v0, v25

    if-ne v0, v3, :cond_1d

    .line 465
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 467
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 468
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 470
    const-string v3, "no"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 471
    .local v4, "no":I
    const-string v3, "name"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 472
    .local v5, "name":Ljava/lang/String;
    const-string v3, "apn"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 473
    .local v6, "apn":Ljava/lang/String;
    const-string v3, "user"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 474
    .local v7, "user":Ljava/lang/String;
    const-string v3, "pwd"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 475
    .restart local v8    # "pwd":Ljava/lang/String;
    const-string v3, "auth"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 477
    .local v9, "auth":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanApnController:Lcom/jetty2m/device/WanApnController;

    invoke-virtual/range {v3 .. v9}, Lcom/jetty2m/device/WanApnController;->addAPN2(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 479
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 481
    .end local v4    # "no":I
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "apn":Ljava/lang/String;
    .end local v7    # "user":Ljava/lang/String;
    .end local v8    # "pwd":Ljava/lang/String;
    .end local v9    # "auth":I
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    :cond_1d
    const/16 v3, 0x3fa

    move/from16 v0, v25

    if-ne v0, v3, :cond_1e

    .line 482
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 484
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 485
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 487
    const-string v3, "profile_num"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v38

    .line 488
    .local v38, "profile_num":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanApnController:Lcom/jetty2m/device/WanApnController;

    move/from16 v0, v38

    invoke-virtual {v3, v0}, Lcom/jetty2m/device/WanApnController;->setCurrentAPN(I)V

    .line 490
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    move/from16 v0, v38

    iput v0, v3, Lcom/jetty2m/config/MifiConfiguration;->WANdefaultAPN:I

    .line 491
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jetty2m/config/MifiConfiguration;->saveToFile()V

    .line 493
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 496
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v38    # "profile_num":I
    :cond_1e
    const/16 v3, 0x3fc

    move/from16 v0, v25

    if-ne v0, v3, :cond_20

    .line 497
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 498
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "oldpwd"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 499
    .local v36, "oldpwd":Ljava/lang/String;
    const-string v3, "newpwd"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 501
    .local v35, "newpwd":Ljava/lang/String;
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    iget-object v3, v3, Lcom/jetty2m/config/MifiConfiguration;->loginPassword:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 502
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 503
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 505
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    move-object/from16 v0, v35

    iput-object v0, v3, Lcom/jetty2m/config/MifiConfiguration;->loginPassword:Ljava/lang/String;

    .line 506
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jetty2m/config/MifiConfiguration;->saveToFile()V

    .line 512
    :goto_a
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 508
    :cond_1f
    const-string v3, "flag"

    const-string v48, "0"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 509
    const-string v3, "error_info"

    const-string v48, "old password error"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_a

    .line 515
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v35    # "newpwd":Ljava/lang/String;
    .end local v36    # "oldpwd":Ljava/lang/String;
    :cond_20
    const/16 v3, 0x3fd

    move/from16 v0, v25

    if-ne v0, v3, :cond_22

    .line 516
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 518
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 519
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 521
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 523
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v48, "adb_enabled"

    move-object/from16 v0, v48

    invoke-static {v3, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v23

    .line 526
    .local v23, "enable":I
    const-string v48, "mode"

    if-lez v23, :cond_21

    const-string v3, "1"

    :goto_b
    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 528
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 529
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 530
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 532
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 526
    .end local v29    # "jsonArray":Lorg/json/JSONArray;
    :cond_21
    const-string v3, "0"

    goto :goto_b

    .line 534
    .end local v23    # "enable":I
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v41    # "resultObject":Lorg/json/JSONObject;
    :cond_22
    const/16 v3, 0x3fe

    move/from16 v0, v25

    if-ne v0, v3, :cond_24

    .line 535
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 537
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 538
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 540
    const-string v3, "mode"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v23

    .line 541
    .restart local v23    # "enable":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v48

    const-string v49, "adb_enabled"

    if-lez v23, :cond_23

    const/4 v3, 0x1

    :goto_c
    move-object/from16 v0, v48

    move-object/from16 v1, v49

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 544
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 541
    :cond_23
    const/4 v3, 0x0

    goto :goto_c

    .line 545
    .end local v23    # "enable":I
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    :cond_24
    const/16 v3, 0x3ff

    move/from16 v0, v25

    if-ne v0, v3, :cond_26

    .line 546
    const-string v3, "sim_slot"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v43

    .line 547
    .local v43, "sim_slot":I
    const-string v3, "pwd"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 548
    .restart local v8    # "pwd":Ljava/lang/String;
    const-string v3, "Ajax"

    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    const-string v49, "sim_slot_get: "

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    move-object/from16 v0, v48

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 551
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 552
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 555
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WanDataController;->getIMEI()Ljava/lang/String;

    move-result-object v3

    const/16 v48, 0x9

    const/16 v49, 0xf

    move/from16 v0, v48

    move/from16 v1, v49

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v39

    .line 556
    .local v39, "raw":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->toCharArray()[C

    move-result-object v18

    .line 557
    .local v18, "codeIMEI":[C
    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->toCharArray()[C

    move-result-object v40

    .line 559
    .local v40, "rawIMEI":[C
    const/4 v3, 0x0

    aget-char v3, v18, v3

    const/16 v48, 0x0

    aget-char v48, v40, v48

    move/from16 v0, v48

    if-ne v3, v0, :cond_25

    const/4 v3, 0x1

    aget-char v3, v18, v3

    const/16 v48, 0x1

    aget-char v48, v40, v48

    move/from16 v0, v48

    if-ne v3, v0, :cond_25

    const/4 v3, 0x2

    aget-char v3, v18, v3

    const/16 v48, 0x30

    move/from16 v0, v48

    if-ne v3, v0, :cond_25

    const/4 v3, 0x3

    aget-char v3, v18, v3

    const/16 v48, 0x3

    aget-char v48, v40, v48

    move/from16 v0, v48

    if-ne v3, v0, :cond_25

    .line 581
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move/from16 v0, v43

    invoke-virtual {v3, v0}, Lcom/jetty2m/device/WanDataController;->setSimCardSlot(I)V

    .line 582
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    move/from16 v0, v43

    iput v0, v3, Lcom/jetty2m/config/MifiConfiguration;->SimcardSlot:I

    .line 583
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jetty2m/config/MifiConfiguration;->saveToFile()V

    .line 584
    const-string v3, "Ajax"

    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    const-string v49, "sim_slot_set: "

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Lcom/jetty2m/config/MifiConfiguration;->getSimCardSlot()I

    move-result v49

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    invoke-virtual {v3}, Lcom/jetty2m/device/DeviceController;->rebootDevice()V

    .line 593
    :cond_25
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 594
    .end local v8    # "pwd":Ljava/lang/String;
    .end local v18    # "codeIMEI":[C
    .end local v30    # "jsonObject":Lorg/json/JSONObject;
    .end local v39    # "raw":Ljava/lang/String;
    .end local v40    # "rawIMEI":[C
    .end local v43    # "sim_slot":I
    :cond_26
    const/16 v3, 0x400

    move/from16 v0, v25

    if-ne v0, v3, :cond_2

    .line 595
    new-instance v30, Lorg/json/JSONObject;

    invoke-direct/range {v30 .. v30}, Lorg/json/JSONObject;-><init>()V

    .line 596
    .restart local v30    # "jsonObject":Lorg/json/JSONObject;
    const-string v3, "flag"

    const-string v48, "1"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 597
    const-string v3, "error_info"

    const-string v48, "none"

    move-object/from16 v0, v30

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 599
    const-string v3, "newimei"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 602
    .local v34, "new_imei":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v0, v34

    invoke-virtual {v3, v0}, Lcom/jetty2m/device/WanDataController;->setNewImei(Ljava/lang/String;)V

    .line 604
    new-instance v41, Lorg/json/JSONObject;

    invoke-direct/range {v41 .. v41}, Lorg/json/JSONObject;-><init>()V

    .line 605
    .restart local v41    # "resultObject":Lorg/json/JSONObject;
    const-string v3, "imei"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/server/AjaxSevlet;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/jetty2m/device/WanDataController;->getIMEI()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 607
    new-instance v29, Lorg/json/JSONArray;

    invoke-direct/range {v29 .. v29}, Lorg/json/JSONArray;-><init>()V

    .line 608
    .restart local v29    # "jsonArray":Lorg/json/JSONArray;
    move-object/from16 v0, v29

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 609
    const-string v3, "results"

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 611
    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->getWriter()Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method
