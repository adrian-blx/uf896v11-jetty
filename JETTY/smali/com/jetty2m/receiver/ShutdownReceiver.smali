.class public Lcom/jetty2m/receiver/ShutdownReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ShutdownReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 20
    const-string v0, "ShutdownReceiver"

    const-string v1, "shutdowning ..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v0

    const/16 v1, 0x5f

    invoke-virtual {v0, v1, v3, v2}, Lcom/jetty2m/device/LedController;->setModeReal(IIZ)V

    .line 23
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v0

    const/16 v1, 0x62

    invoke-virtual {v0, v1, v3, v2}, Lcom/jetty2m/device/LedController;->setModeReal(IIZ)V

    .line 24
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/jetty2m/device/LedController;->setModeReal(IIZ)V

    .line 25
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v2}, Lcom/jetty2m/device/LedController;->setModeReal(IIZ)V

    .line 26
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v0

    invoke-virtual {v0, v3, v3, v2}, Lcom/jetty2m/device/LedController;->setModeReal(IIZ)V

    .line 27
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3, v2}, Lcom/jetty2m/device/LedController;->setModeReal(IIZ)V

    .line 29
    return-void
.end method
