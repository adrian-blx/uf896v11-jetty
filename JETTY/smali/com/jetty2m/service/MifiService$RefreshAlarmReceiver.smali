.class public Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MifiService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jetty2m/service/MifiService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RefreshAlarmReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jetty2m/service/MifiService;


# direct methods
.method public constructor <init>(Lcom/jetty2m/service/MifiService;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x3

    .line 242
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    iget-object v1, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v1}, Lcom/jetty2m/service/MifiService;->access$100(Lcom/jetty2m/service/MifiService;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/jetty2m/service/MifiService;->access$002(Lcom/jetty2m/service/MifiService;Z)Z

    .line 244
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$000(Lcom/jetty2m/service/MifiService;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$200(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WanDataController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/device/WanDataController;->getNetStatus()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Connected"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$300(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WifiApController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$300(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WifiApController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    iget-object v1, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v1}, Lcom/jetty2m/service/MifiService;->access$400(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v1

    iget-object v1, v1, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    iget-object v2, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v2}, Lcom/jetty2m/service/MifiService;->access$400(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v2

    iget-object v2, v2, Lcom/jetty2m/config/MifiConfiguration;->WIFIpassword:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/jetty2m/service/MifiService;->HandleBindCommand(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$300(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WifiApController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v0

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    iget-object v0, v0, Lcom/jetty2m/service/MifiService;->mServer:Lcom/jetty2m/server/JServer;

    invoke-virtual {v0}, Lcom/jetty2m/server/JServer;->isStarted()Z

    move-result v0

    if-nez v0, :cond_3

    .line 256
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    iget-object v0, v0, Lcom/jetty2m/service/MifiService;->mServer:Lcom/jetty2m/server/JServer;

    iget-object v1, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-virtual {v1}, Lcom/jetty2m/service/MifiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jetty2m/server/JServer;->start(Landroid/content/Context;)V

    .line 273
    :cond_3
    :goto_0
    return-void

    .line 259
    :cond_4
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$300(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WifiApController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v0

    if-ne v0, v3, :cond_5

    .line 260
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-virtual {v0}, Lcom/jetty2m/service/MifiService;->HandleUnBindCommand()V

    .line 264
    :cond_5
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$200(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WanDataController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/device/WanDataController;->getMobileDataEnable()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 265
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$200(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WanDataController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jetty2m/device/WanDataController;->setMobileDataEnable(Z)V

    .line 269
    :cond_6
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    iget-object v0, v0, Lcom/jetty2m/service/MifiService;->mServer:Lcom/jetty2m/server/JServer;

    invoke-virtual {v0}, Lcom/jetty2m/server/JServer;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 270
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    iget-object v0, v0, Lcom/jetty2m/service/MifiService;->mServer:Lcom/jetty2m/server/JServer;

    invoke-virtual {v0}, Lcom/jetty2m/server/JServer;->stop()V

    goto :goto_0
.end method
