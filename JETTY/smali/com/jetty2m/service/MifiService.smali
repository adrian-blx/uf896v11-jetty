.class public Lcom/jetty2m/service/MifiService;
.super Landroid/app/Service;
.source "MifiService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jetty2m/service/MifiService$KeyReceiver;,
        Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;
    }
.end annotation


# static fields
.field private static context:Landroid/content/Context;


# instance fields
.field private alarmManager:Landroid/app/AlarmManager;

.field private licensed:Z

.field private mDeviceController:Lcom/jetty2m/device/DeviceController;

.field private mGpsController:Lcom/jetty2m/device/GpsController;

.field mServer:Lcom/jetty2m/server/JServer;

.field private mWanApnController:Lcom/jetty2m/device/WanApnController;

.field private mWanDataController:Lcom/jetty2m/device/WanDataController;

.field private mWifiApController:Lcom/jetty2m/device/WifiApController;

.field private mifiConfiguration:Lcom/jetty2m/config/MifiConfiguration;

.field private timerIntent:Landroid/app/PendingIntent;

.field private timerReceiver:Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;

.field private wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jetty2m/service/MifiService;->licensed:Z

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jetty2m/service/MifiService;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 81
    new-instance v0, Lcom/jetty2m/server/JServer;

    const/16 v1, 0x1f90

    invoke-direct {v0, v1}, Lcom/jetty2m/server/JServer;-><init>(I)V

    iput-object v0, p0, Lcom/jetty2m/service/MifiService;->mServer:Lcom/jetty2m/server/JServer;

    .line 277
    return-void
.end method

.method static synthetic access$000(Lcom/jetty2m/service/MifiService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/service/MifiService;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/jetty2m/service/MifiService;->licensed:Z

    return v0
.end method

.method static synthetic access$002(Lcom/jetty2m/service/MifiService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/jetty2m/service/MifiService;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/jetty2m/service/MifiService;->licensed:Z

    return p1
.end method

.method static synthetic access$100(Lcom/jetty2m/service/MifiService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/service/MifiService;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/jetty2m/service/MifiService;->isLicensed()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WanDataController;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/service/MifiService;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/jetty2m/service/MifiService;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WifiApController;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/service/MifiService;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/jetty2m/service/MifiService;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/config/MifiConfiguration;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/service/MifiService;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/jetty2m/service/MifiService;->mifiConfiguration:Lcom/jetty2m/config/MifiConfiguration;

    return-object v0
.end method

.method static synthetic access$500(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/DeviceController;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/service/MifiService;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/jetty2m/service/MifiService;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    return-object v0
.end method

.method private acquireWakeLock()V
    .locals 3

    .prologue
    .line 188
    iget-object v1, p0, Lcom/jetty2m/service/MifiService;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 190
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/jetty2m/service/MifiService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 191
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x20000001

    const-string v2, "MifiService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/jetty2m/service/MifiService;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 192
    iget-object v1, p0, Lcom/jetty2m/service/MifiService;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/jetty2m/service/MifiService;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 197
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/jetty2m/service/MifiService;->context:Landroid/content/Context;

    return-object v0
.end method

.method private isLicensed()Z
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x1

    return v0
.end method

.method private restartTimer()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 219
    iget-object v0, p0, Lcom/jetty2m/service/MifiService;->timerIntent:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    .line 220
    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.mifiservice.TIMER_UPDATE"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 221
    .local v7, "intent":Landroid/content/Intent;
    invoke-static {p0, v1, v7, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/jetty2m/service/MifiService;->timerIntent:Landroid/app/PendingIntent;

    .line 226
    .end local v7    # "intent":Landroid/content/Intent;
    :goto_0
    const/16 v8, 0x4e20

    .line 227
    .local v8, "time":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v4, 0x61a8

    add-long v2, v0, v4

    .line 232
    .local v2, "initialRefreshTime":J
    iget-object v0, p0, Lcom/jetty2m/service/MifiService;->alarmManager:Landroid/app/AlarmManager;

    const/4 v1, 0x2

    int-to-long v4, v8

    iget-object v6, p0, Lcom/jetty2m/service/MifiService;->timerIntent:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 234
    return-void

    .line 223
    .end local v2    # "initialRefreshTime":J
    .end local v8    # "time":I
    :cond_0
    iget-object v0, p0, Lcom/jetty2m/service/MifiService;->alarmManager:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/jetty2m/service/MifiService;->timerIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method public HandleBindCommand(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 317
    new-instance v0, Lcom/jetty2m/service/MifiService$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/jetty2m/service/MifiService$2;-><init>(Lcom/jetty2m/service/MifiService;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jetty2m/service/MifiService$2;->start()V

    .line 332
    return-void
.end method

.method public HandleUnBindCommand()V
    .locals 1

    .prologue
    .line 336
    new-instance v0, Lcom/jetty2m/service/MifiService$3;

    invoke-direct {v0, p0}, Lcom/jetty2m/service/MifiService$3;-><init>(Lcom/jetty2m/service/MifiService;)V

    invoke-virtual {v0}, Lcom/jetty2m/service/MifiService$3;->start()V

    .line 344
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 100
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 102
    sput-object p0, Lcom/jetty2m/service/MifiService;->context:Landroid/content/Context;

    .line 104
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mifiservice.TIMER_UPDATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 105
    .local v2, "timerFilter":Landroid/content/IntentFilter;
    new-instance v3, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;

    invoke-direct {v3, p0}, Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;-><init>(Lcom/jetty2m/service/MifiService;)V

    iput-object v3, p0, Lcom/jetty2m/service/MifiService;->timerReceiver:Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;

    .line 106
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->timerReceiver:Lcom/jetty2m/service/MifiService$RefreshAlarmReceiver;

    invoke-virtual {p0, v3, v2}, Lcom/jetty2m/service/MifiService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 109
    new-instance v0, Lcom/jetty2m/service/MifiService$KeyReceiver;

    invoke-direct {v0, p0}, Lcom/jetty2m/service/MifiService$KeyReceiver;-><init>(Lcom/jetty2m/service/MifiService;)V

    .line 110
    .local v0, "keyReceiver":Lcom/jetty2m/service/MifiService$KeyReceiver;
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 111
    .local v1, "keyfilter":Landroid/content/IntentFilter;
    const-string v3, "com.android.mifi.restore.down"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0, v0, v1}, Lcom/jetty2m/service/MifiService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 114
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/jetty2m/service/MifiService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/AlarmManager;

    iput-object v3, p0, Lcom/jetty2m/service/MifiService;->alarmManager:Landroid/app/AlarmManager;

    .line 115
    invoke-direct {p0}, Lcom/jetty2m/service/MifiService;->restartTimer()V

    .line 117
    invoke-static {}, Lcom/jetty2m/device/WifiApController;->getInstance()Lcom/jetty2m/device/WifiApController;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/service/MifiService;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    .line 118
    invoke-static {}, Lcom/jetty2m/device/WanDataController;->getInstance()Lcom/jetty2m/device/WanDataController;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/service/MifiService;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    .line 119
    invoke-static {}, Lcom/jetty2m/device/DeviceController;->getInstance()Lcom/jetty2m/device/DeviceController;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/service/MifiService;->mDeviceController:Lcom/jetty2m/device/DeviceController;

    .line 120
    invoke-static {}, Lcom/jetty2m/device/WanApnController;->getInstance()Lcom/jetty2m/device/WanApnController;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/service/MifiService;->mWanApnController:Lcom/jetty2m/device/WanApnController;

    .line 121
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/service/MifiService;->mifiConfiguration:Lcom/jetty2m/config/MifiConfiguration;

    .line 123
    invoke-static {}, Lcom/jetty2m/device/GpsController;->getInstance()Lcom/jetty2m/device/GpsController;

    move-result-object v3

    iput-object v3, p0, Lcom/jetty2m/service/MifiService;->mGpsController:Lcom/jetty2m/device/GpsController;

    .line 136
    const-wide/16 v3, 0x1f4

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :goto_0
    invoke-direct {p0}, Lcom/jetty2m/service/MifiService;->isLicensed()Z

    move-result v3

    iput-boolean v3, p0, Lcom/jetty2m/service/MifiService;->licensed:Z

    .line 150
    iget-boolean v3, p0, Lcom/jetty2m/service/MifiService;->licensed:Z

    if-eqz v3, :cond_4

    .line 152
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mWifiApController:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 153
    :cond_0
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mifiConfiguration:Lcom/jetty2m/config/MifiConfiguration;

    iget-object v3, v3, Lcom/jetty2m/config/MifiConfiguration;->WIFIssid:Ljava/lang/String;

    iget-object v4, p0, Lcom/jetty2m/service/MifiService;->mifiConfiguration:Lcom/jetty2m/config/MifiConfiguration;

    iget-object v4, v4, Lcom/jetty2m/config/MifiConfiguration;->WIFIpassword:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/jetty2m/service/MifiService;->HandleBindCommand(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_1
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    iget-object v4, p0, Lcom/jetty2m/service/MifiService;->mifiConfiguration:Lcom/jetty2m/config/MifiConfiguration;

    iget v4, v4, Lcom/jetty2m/config/MifiConfiguration;->WANnetworkType:I

    invoke-virtual {v3, v4}, Lcom/jetty2m/device/WanDataController;->setPreferedNetworkType(I)V

    .line 157
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v4

    iget-boolean v4, v4, Lcom/jetty2m/config/MifiConfiguration;->WANautoConnect:Z

    invoke-virtual {v3, v4}, Lcom/jetty2m/device/WanDataController;->setMobileDataEnable(Z)V

    .line 158
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WanDataController;->updaterndisState()V

    .line 159
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    iget v3, v3, Lcom/jetty2m/config/MifiConfiguration;->WANdefaultAPN:I

    if-lez v3, :cond_2

    .line 160
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mWanApnController:Lcom/jetty2m/device/WanApnController;

    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v4

    iget v4, v4, Lcom/jetty2m/config/MifiConfiguration;->WANdefaultAPN:I

    invoke-virtual {v3, v4}, Lcom/jetty2m/device/WanApnController;->setCurrentAPN(I)V

    .line 165
    :cond_2
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mServer:Lcom/jetty2m/server/JServer;

    invoke-virtual {v3}, Lcom/jetty2m/server/JServer;->isStarted()Z

    move-result v3

    if-nez v3, :cond_3

    .line 166
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mServer:Lcom/jetty2m/server/JServer;

    invoke-virtual {p0}, Lcom/jetty2m/service/MifiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jetty2m/server/JServer;->start(Landroid/content/Context;)V

    .line 173
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/jetty2m/service/MifiService;->acquireWakeLock()V

    .line 174
    return-void

    .line 169
    :cond_4
    iget-object v3, p0, Lcom/jetty2m/service/MifiService;->mWanDataController:Lcom/jetty2m/device/WanDataController;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/jetty2m/device/WanDataController;->setMobileDataEnable(Z)V

    goto :goto_1

    .line 137
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/jetty2m/service/MifiService;->timerIntent:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/jetty2m/service/MifiService;->alarmManager:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/jetty2m/service/MifiService;->timerIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 183
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 184
    return-void
.end method
