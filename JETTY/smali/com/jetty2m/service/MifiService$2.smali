.class Lcom/jetty2m/service/MifiService$2;
.super Ljava/lang/Thread;
.source "MifiService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jetty2m/service/MifiService;->HandleBindCommand(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jetty2m/service/MifiService;

.field final synthetic val$password:Ljava/lang/String;

.field final synthetic val$ssid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/jetty2m/service/MifiService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/jetty2m/service/MifiService$2;->this$0:Lcom/jetty2m/service/MifiService;

    iput-object p2, p0, Lcom/jetty2m/service/MifiService$2;->val$ssid:Ljava/lang/String;

    iput-object p3, p0, Lcom/jetty2m/service/MifiService$2;->val$password:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 322
    iget-object v1, p0, Lcom/jetty2m/service/MifiService$2;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v1}, Lcom/jetty2m/service/MifiService;->access$300(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WifiApController;

    move-result-object v1

    iget-object v2, p0, Lcom/jetty2m/service/MifiService$2;->val$ssid:Ljava/lang/String;

    iget-object v3, p0, Lcom/jetty2m/service/MifiService$2;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v3}, Lcom/jetty2m/service/MifiService;->access$400(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v3

    iget v3, v3, Lcom/jetty2m/config/MifiConfiguration;->WIFIencrypt:I

    iget-object v4, p0, Lcom/jetty2m/service/MifiService$2;->val$password:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/jetty2m/device/WifiApController;->setWifiApConfig(Ljava/lang/String;ILjava/lang/String;)Z

    .line 323
    iget-object v1, p0, Lcom/jetty2m/service/MifiService$2;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v1}, Lcom/jetty2m/service/MifiService;->access$300(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WifiApController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 324
    .local v0, "config":Landroid/net/wifi/WifiConfiguration;
    iget-object v1, p0, Lcom/jetty2m/service/MifiService$2;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v1}, Lcom/jetty2m/service/MifiService;->access$400(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v1

    iget-boolean v1, v1, Lcom/jetty2m/config/MifiConfiguration;->WIFIssidhidden:Z

    iput-boolean v1, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    .line 325
    iget-object v1, p0, Lcom/jetty2m/service/MifiService$2;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v1}, Lcom/jetty2m/service/MifiService;->access$400(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v1

    iget v1, v1, Lcom/jetty2m/config/MifiConfiguration;->WIFIchannel:I

    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->channel:I

    .line 326
    iget-object v1, p0, Lcom/jetty2m/service/MifiService$2;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v1}, Lcom/jetty2m/service/MifiService;->access$400(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v1

    iget-object v1, v1, Lcom/jetty2m/config/MifiConfiguration;->WIFIhwmode:Ljava/lang/String;

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->hwMode:Ljava/lang/String;

    .line 327
    iget-object v1, p0, Lcom/jetty2m/service/MifiService$2;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v1}, Lcom/jetty2m/service/MifiService;->access$400(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v1

    iget v1, v1, Lcom/jetty2m/config/MifiConfiguration;->WIFImaxSta:I

    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->maxSta:I

    .line 328
    iget-object v1, p0, Lcom/jetty2m/service/MifiService$2;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v1}, Lcom/jetty2m/service/MifiService;->access$300(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WifiApController;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/jetty2m/device/WifiApController;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)I

    .line 329
    return-void
.end method
