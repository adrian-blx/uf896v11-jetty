.class public Lcom/jetty2m/service/MifiService$KeyReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MifiService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jetty2m/service/MifiService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "KeyReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jetty2m/service/MifiService;


# direct methods
.method public constructor <init>(Lcom/jetty2m/service/MifiService;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/jetty2m/service/MifiService$KeyReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 280
    const-string v0, "MifiService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Restore KeyReceiver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.mifi.restore.down"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$KeyReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$300(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/WifiApController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$KeyReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-virtual {v0}, Lcom/jetty2m/service/MifiService;->HandleUnBindCommand()V

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/jetty2m/service/MifiService$KeyReceiver;->this$0:Lcom/jetty2m/service/MifiService;

    invoke-static {v0}, Lcom/jetty2m/service/MifiService;->access$500(Lcom/jetty2m/service/MifiService;)Lcom/jetty2m/device/DeviceController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/device/DeviceController;->restoreDevice()V

    .line 287
    :cond_1
    return-void
.end method
