.class public Lcom/jetty2m/device/WanDataController;
.super Ljava/lang/Object;
.source "WanDataController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jetty2m/device/WanDataController$1;,
        Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;,
        Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;
    }
.end annotation


# static fields
.field public static UsbTethering_enabled:I

.field private static instance:Lcom/jetty2m/device/WanDataController;


# instance fields
.field private connectivityManager:Landroid/net/ConnectivityManager;

.field private imeiString:Ljava/lang/String;

.field last_network_type:I

.field private last_signallevel:I

.field private mContext:Landroid/content/Context;

.field private myListener:Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;

.field private telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/jetty2m/device/WanDataController;->instance:Lcom/jetty2m/device/WanDataController;

    .line 64
    const/4 v0, 0x0

    sput v0, Lcom/jetty2m/device/WanDataController;->UsbTethering_enabled:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const-string v1, "0000"

    iput-object v1, p0, Lcom/jetty2m/device/WanDataController;->imeiString:Ljava/lang/String;

    .line 63
    iput v3, p0, Lcom/jetty2m/device/WanDataController;->last_network_type:I

    .line 67
    iput-object v2, p0, Lcom/jetty2m/device/WanDataController;->myListener:Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;

    .line 68
    iput v3, p0, Lcom/jetty2m/device/WanDataController;->last_signallevel:I

    .line 69
    iput-object v2, p0, Lcom/jetty2m/device/WanDataController;->mContext:Landroid/content/Context;

    .line 97
    const-string v1, "phone"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 98
    const-string v1, "connectivity"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    iput-object v1, p0, Lcom/jetty2m/device/WanDataController;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 99
    iput-object p1, p0, Lcom/jetty2m/device/WanDataController;->mContext:Landroid/content/Context;

    .line 100
    iget-object v1, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/jetty2m/device/WanDataController;->imeiString:Ljava/lang/String;

    .line 102
    new-instance v0, Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;

    invoke-direct {v0, p0, v2}, Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;-><init>(Lcom/jetty2m/device/WanDataController;Lcom/jetty2m/device/WanDataController$1;)V

    .line 103
    .local v0, "myListener":Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;
    iget-object v1, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    const/16 v2, 0x141

    invoke-virtual {v1, v0, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 105
    invoke-virtual {p0}, Lcom/jetty2m/device/WanDataController;->registerData()V

    .line 107
    return-void
.end method

.method static synthetic access$002(Lcom/jetty2m/device/WanDataController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/jetty2m/device/WanDataController;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/jetty2m/device/WanDataController;->last_signallevel:I

    return p1
.end method

.method static synthetic access$100(Lcom/jetty2m/device/WanDataController;I)V
    .locals 0
    .param p0, "x0"    # Lcom/jetty2m/device/WanDataController;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/jetty2m/device/WanDataController;->updateNetworkType(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/jetty2m/device/WanDataController;)Landroid/net/ConnectivityManager;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/device/WanDataController;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController;->connectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jetty2m/device/WanDataController;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/device/WanDataController;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method public static getInstance()Lcom/jetty2m/device/WanDataController;
    .locals 2

    .prologue
    .line 110
    sget-object v0, Lcom/jetty2m/device/WanDataController;->instance:Lcom/jetty2m/device/WanDataController;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Lcom/jetty2m/device/WanDataController;

    invoke-static {}, Lcom/jetty2m/service/MifiService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jetty2m/device/WanDataController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/jetty2m/device/WanDataController;->instance:Lcom/jetty2m/device/WanDataController;

    .line 113
    :cond_0
    sget-object v0, Lcom/jetty2m/device/WanDataController;->instance:Lcom/jetty2m/device/WanDataController;

    return-object v0
.end method

.method private updateNetworkType(I)V
    .locals 4
    .param p1, "network_type"    # I

    .prologue
    .line 415
    const/4 v0, 0x0

    .line 416
    .local v0, "type":I
    const-string v1, "WanDataController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ConnectivityManager updateNetworkType "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    packed-switch p1, :pswitch_data_0

    .line 444
    const/4 v0, 0x0

    .line 449
    :goto_0
    iget v1, p0, Lcom/jetty2m/device/WanDataController;->last_network_type:I

    if-eq v1, v0, :cond_1

    .line 451
    if-nez v0, :cond_2

    .line 468
    :cond_0
    :goto_1
    iput v0, p0, Lcom/jetty2m/device/WanDataController;->last_network_type:I

    .line 473
    :cond_1
    return-void

    .line 424
    :pswitch_0
    const/4 v0, 0x2

    .line 425
    goto :goto_0

    .line 436
    :pswitch_1
    const/4 v0, 0x3

    .line 437
    goto :goto_0

    .line 440
    :pswitch_2
    const/4 v0, 0x4

    .line 441
    goto :goto_0

    .line 455
    :cond_2
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 459
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 462
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_1

    .line 418
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getICCID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIMEI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jetty2m/device/WanDataController;->imeiString:Ljava/lang/String;

    .line 281
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController;->imeiString:Ljava/lang/String;

    return-object v0
.end method

.method public getIMSI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMobileDataEnable()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v0

    return v0
.end method

.method public getMobileIP()Ljava/lang/String;
    .locals 8

    .prologue
    .line 165
    const-string v4, "0.0.0.0"

    .line 168
    .local v4, "ip":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 169
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/NetworkInterface;

    .line 170
    .local v3, "intf":Ljava/net/NetworkInterface;
    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .local v1, "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 171
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/net/InetAddress;

    .line 172
    .local v2, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v2}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v6

    if-nez v6, :cond_1

    instance-of v6, v2, Ljava/net/Inet4Address;

    if-eqz v6, :cond_1

    .line 174
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    .line 176
    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "rmnet_data"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_1

    move-object v5, v4

    .line 185
    .end local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v1    # "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v2    # "inetAddress":Ljava/net/InetAddress;
    .end local v3    # "intf":Ljava/net/NetworkInterface;
    .end local v4    # "ip":Ljava/lang/String;
    .local v5, "ip":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 182
    .end local v5    # "ip":Ljava/lang/String;
    .restart local v4    # "ip":Ljava/lang/String;
    :catch_0
    move-exception v6

    :cond_2
    move-object v5, v4

    .line 185
    .end local v4    # "ip":Ljava/lang/String;
    .restart local v5    # "ip":Ljava/lang/String;
    goto :goto_0
.end method

.method public getMobileRxBytes()J
    .locals 4

    .prologue
    .line 328
    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getMobileTxBytes()J
    .locals 4

    .prologue
    .line 332
    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Landroid/net/TrafficStats;->getMobileTxBytes()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getNetMode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 198
    iget-object v1, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    .line 200
    .local v0, "type":I
    if-nez v0, :cond_0

    .line 201
    iget-object v1, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getVoiceNetworkType()I

    move-result v0

    .line 203
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 235
    const-string v1, "UNKNOWN"

    :goto_0
    return-object v1

    .line 205
    :pswitch_0
    const-string v1, "GPRS"

    goto :goto_0

    .line 207
    :pswitch_1
    const-string v1, "EDGE"

    goto :goto_0

    .line 209
    :pswitch_2
    const-string v1, "UMTS"

    goto :goto_0

    .line 211
    :pswitch_3
    const-string v1, "HSDPA"

    goto :goto_0

    .line 213
    :pswitch_4
    const-string v1, "HSUPA"

    goto :goto_0

    .line 215
    :pswitch_5
    const-string v1, "HSPA"

    goto :goto_0

    .line 217
    :pswitch_6
    const-string v1, "CDMA"

    goto :goto_0

    .line 219
    :pswitch_7
    const-string v1, "CDMA - EvDo rev. 0"

    goto :goto_0

    .line 221
    :pswitch_8
    const-string v1, "CDMA - EvDo rev. A"

    goto :goto_0

    .line 223
    :pswitch_9
    const-string v1, "CDMA - EvDo rev. B"

    goto :goto_0

    .line 225
    :pswitch_a
    const-string v1, "CDMA - 1xRTT"

    goto :goto_0

    .line 227
    :pswitch_b
    const-string v1, "LTE"

    goto :goto_0

    .line 229
    :pswitch_c
    const-string v1, "CDMA - eHRPD"

    goto :goto_0

    .line 231
    :pswitch_d
    const-string v1, "iDEN"

    goto :goto_0

    .line 233
    :pswitch_e
    const-string v1, "HSPA+"

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_d
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_e
    .end packed-switch
.end method

.method public getNetStatus()Ljava/lang/String;
    .locals 5

    .prologue
    .line 243
    const/4 v1, 0x0

    .line 244
    .local v1, "info":Landroid/net/NetworkInfo;
    iget-object v4, p0, Lcom/jetty2m/device/WanDataController;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v2

    .line 245
    .local v2, "infos":[Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    .line 246
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 247
    aget-object v4, v2, v0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-nez v4, :cond_0

    .line 248
    aget-object v1, v2, v0

    .line 246
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 253
    .end local v0    # "i":I
    :cond_1
    if-eqz v1, :cond_5

    .line 254
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    .line 256
    .local v3, "state":Landroid/net/NetworkInfo$State;
    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v3, v4, :cond_2

    .line 257
    const-string v4, "Disconnected"

    .line 267
    .end local v3    # "state":Landroid/net/NetworkInfo$State;
    :goto_1
    return-object v4

    .line 258
    .restart local v3    # "state":Landroid/net/NetworkInfo$State;
    :cond_2
    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v3, v4, :cond_3

    .line 259
    const-string v4, "Connecting"

    goto :goto_1

    .line 260
    :cond_3
    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v3, v4, :cond_4

    .line 261
    const-string v4, "Connected"

    goto :goto_1

    .line 262
    :cond_4
    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v3, v4, :cond_5

    .line 263
    const-string v4, "Disconnecting"

    goto :goto_1

    .line 267
    .end local v3    # "state":Landroid/net/NetworkInfo$State;
    :cond_5
    const-string v4, "Disconnected"

    goto :goto_1
.end method

.method public getPreferedNetworkType()I
    .locals 5

    .prologue
    .line 549
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jetty2m/config/MifiConfiguration;->getnetworktype()I

    move-result v1

    .line 550
    .local v1, "type":I
    const-string v2, "WanDataController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPreferredNetworkType "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    const/4 v0, 0x0

    .line 552
    .local v0, "mode":I
    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    .line 553
    const/4 v0, 0x0

    .line 560
    :cond_0
    :goto_0
    return v0

    .line 554
    :cond_1
    const/16 v2, 0x15

    if-ne v1, v2, :cond_2

    .line 555
    const/4 v0, 0x1

    goto :goto_0

    .line 556
    :cond_2
    const/16 v2, 0xb

    if-ne v1, v2, :cond_0

    .line 557
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getSIMStatus()Ljava/lang/String;
    .locals 2

    .prologue
    .line 307
    iget-object v1, p0, Lcom/jetty2m/device/WanDataController;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    .line 309
    .local v0, "simstate":I
    if-nez v0, :cond_0

    .line 310
    const-string v1, "Unknown"

    .line 322
    :goto_0
    return-object v1

    .line 311
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 312
    const-string v1, "Absent"

    goto :goto_0

    .line 313
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 314
    const-string v1, "Pin Required"

    goto :goto_0

    .line 315
    :cond_2
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 316
    const-string v1, "PUK Required"

    goto :goto_0

    .line 317
    :cond_3
    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 318
    const-string v1, "Network Locked"

    goto :goto_0

    .line 319
    :cond_4
    const/4 v1, 0x5

    if-ne v0, v1, :cond_5

    .line 320
    const-string v1, "Ready"

    goto :goto_0

    .line 322
    :cond_5
    const-string v1, "Unknown"

    goto :goto_0
.end method

.method public getSignalStrength()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 290
    invoke-virtual {p0}, Lcom/jetty2m/device/WanDataController;->getSIMStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Ready"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 291
    iget v1, p0, Lcom/jetty2m/device/WanDataController;->last_signallevel:I

    if-nez v1, :cond_0

    .line 292
    invoke-virtual {p0}, Lcom/jetty2m/device/WanDataController;->getNetMode()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UNKNOWN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 293
    iput v0, p0, Lcom/jetty2m/device/WanDataController;->last_signallevel:I

    .line 299
    :cond_0
    :goto_0
    iget v0, p0, Lcom/jetty2m/device/WanDataController;->last_signallevel:I

    .line 301
    :cond_1
    return v0

    .line 295
    :cond_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/jetty2m/device/WanDataController;->last_signallevel:I

    goto :goto_0
.end method

.method public registerData()V
    .locals 3

    .prologue
    .line 353
    new-instance v1, Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;

    invoke-direct {v1, p0}, Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;-><init>(Lcom/jetty2m/device/WanDataController;)V

    .line 354
    .local v1, "mNetWorkReceiver":Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 355
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 356
    iget-object v2, p0, Lcom/jetty2m/device/WanDataController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 358
    return-void
.end method

.method public setMobileDataEnable(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 126
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    .line 127
    return-void
.end method

.method public setNewImei(Ljava/lang/String;)V
    .locals 4
    .param p1, "newimei"    # Ljava/lang/String;

    .prologue
    .line 590
    const-string v1, "persist.sys.newimei"

    invoke-static {v1, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    const-string v1, "persist.sys.newimei"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 592
    .local v0, "write_imei":Ljava/lang/String;
    const-string v1, "WanDataController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setNewImei "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    return-void
.end method

.method public setPreferedNetworkType(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 524
    const/16 v0, 0xb

    .line 525
    .local v0, "type":I
    if-nez p1, :cond_1

    .line 526
    const/16 v0, 0xc

    .line 533
    :cond_0
    :goto_0
    const-string v1, "WanDataController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPreferredNetworkType "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    return-void

    .line 527
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 528
    const/16 v0, 0x15

    goto :goto_0

    .line 529
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 530
    const/16 v0, 0xb

    goto :goto_0
.end method

.method public setSimCardSlot(I)V
    .locals 4
    .param p1, "simslot"    # I

    .prologue
    .line 576
    const/4 v0, 0x1

    .line 577
    .local v0, "slot":I
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 578
    const/4 v0, 0x1

    .line 579
    const-string v1, "ctl.start"

    const-string v2, "simsel1"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    :cond_0
    :goto_0
    const-string v1, "WanDataController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSimCardSlot "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    return-void

    .line 580
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 581
    const/4 v0, 0x2

    .line 582
    const-string v1, "ctl.start"

    const-string v2, "simsel2"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setUsbTethering(Z)Z
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 567
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->setUsbTethering(Z)I

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    const/4 v0, 0x0

    .line 571
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public updaterndisState()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 477
    invoke-virtual {p0}, Lcom/jetty2m/device/WanDataController;->getIMEI()Ljava/lang/String;

    move-result-object v0

    .line 478
    .local v0, "readimei":Ljava/lang/String;
    const-string v1, "WanDataController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getIMEI:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xf

    if-ne v1, v2, :cond_0

    sget v1, Lcom/jetty2m/device/WanDataController;->UsbTethering_enabled:I

    if-nez v1, :cond_0

    .line 480
    const-string v1, "WanDataController"

    const-string v2, "updaterndisState sucess2 "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    invoke-virtual {p0, v4}, Lcom/jetty2m/device/WanDataController;->setUsbTethering(Z)Z

    .line 482
    sput v4, Lcom/jetty2m/device/WanDataController;->UsbTethering_enabled:I

    .line 485
    :cond_0
    return-void
.end method
