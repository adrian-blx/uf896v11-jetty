.class public Lcom/jetty2m/device/DeviceController;
.super Ljava/lang/Object;
.source "DeviceController.java"


# static fields
.field private static instance:Lcom/jetty2m/device/DeviceController;


# instance fields
.field private batteryCapacity:I

.field private isCharging:Z

.field private mContext:Landroid/content/Context;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private pPowerManager:Landroid/os/PowerManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/jetty2m/device/DeviceController;->instance:Lcom/jetty2m/device/DeviceController;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/jetty2m/device/DeviceController;->pPowerManager:Landroid/os/PowerManager;

    .line 27
    iput-object v0, p0, Lcom/jetty2m/device/DeviceController;->mContext:Landroid/content/Context;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jetty2m/device/DeviceController;->isCharging:Z

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lcom/jetty2m/device/DeviceController;->batteryCapacity:I

    .line 43
    new-instance v0, Lcom/jetty2m/device/DeviceController$1;

    invoke-direct {v0, p0}, Lcom/jetty2m/device/DeviceController$1;-><init>(Lcom/jetty2m/device/DeviceController;)V

    iput-object v0, p0, Lcom/jetty2m/device/DeviceController;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 200
    iput-object p1, p0, Lcom/jetty2m/device/DeviceController;->mContext:Landroid/content/Context;

    .line 202
    iget-object v0, p0, Lcom/jetty2m/device/DeviceController;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/jetty2m/device/DeviceController;->pPowerManager:Landroid/os/PowerManager;

    .line 204
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/jetty2m/device/DeviceController;->mIntentFilter:Landroid/content/IntentFilter;

    .line 205
    iget-object v0, p0, Lcom/jetty2m/device/DeviceController;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/jetty2m/device/DeviceController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/jetty2m/device/DeviceController;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/jetty2m/device/DeviceController;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 207
    return-void
.end method

.method static synthetic access$002(Lcom/jetty2m/device/DeviceController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/jetty2m/device/DeviceController;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/jetty2m/device/DeviceController;->batteryCapacity:I

    return p1
.end method

.method static synthetic access$102(Lcom/jetty2m/device/DeviceController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/jetty2m/device/DeviceController;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/jetty2m/device/DeviceController;->isCharging:Z

    return p1
.end method

.method public static getInstance()Lcom/jetty2m/device/DeviceController;
    .locals 2

    .prologue
    .line 210
    sget-object v0, Lcom/jetty2m/device/DeviceController;->instance:Lcom/jetty2m/device/DeviceController;

    if-nez v0, :cond_0

    .line 211
    new-instance v0, Lcom/jetty2m/device/DeviceController;

    invoke-static {}, Lcom/jetty2m/service/MifiService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jetty2m/device/DeviceController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/jetty2m/device/DeviceController;->instance:Lcom/jetty2m/device/DeviceController;

    .line 213
    :cond_0
    sget-object v0, Lcom/jetty2m/device/DeviceController;->instance:Lcom/jetty2m/device/DeviceController;

    return-object v0
.end method


# virtual methods
.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method public getSWVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    sget-object v0, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    return-object v0
.end method

.method public rebootDevice()V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/jetty2m/device/DeviceController;->pPowerManager:Landroid/os/PowerManager;

    const-string v1, "user command"

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 233
    return-void
.end method

.method public restoreDevice()V
    .locals 1

    .prologue
    .line 264
    invoke-static {}, Lcom/jetty2m/config/MifiConfiguration;->getInstance()Lcom/jetty2m/config/MifiConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/config/MifiConfiguration;->reset()V

    .line 266
    invoke-static {}, Lcom/jetty2m/device/WanApnController;->getInstance()Lcom/jetty2m/device/WanApnController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jetty2m/device/WanApnController;->restoreAPN()V

    .line 268
    invoke-virtual {p0}, Lcom/jetty2m/device/DeviceController;->rebootDevice()V

    .line 270
    return-void
.end method
