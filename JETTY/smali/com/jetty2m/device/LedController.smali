.class public Lcom/jetty2m/device/LedController;
.super Ljava/lang/Object;
.source "LedController.java"


# static fields
.field private static instance:Lcom/jetty2m/device/LedController;


# instance fields
.field private blinkGpios:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private blinkGpiosStatus:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private gpioBlinkStatus:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private gpio_current_value:I

.field private handler:Landroid/os/Handler;

.field private mGpioController:Lcom/jetty2m/device/GpioController;

.field task:Ljava/util/TimerTask;

.field timer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/jetty2m/device/LedController;->instance:Lcom/jetty2m/device/LedController;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/jetty2m/device/LedController;->timer:Ljava/util/Timer;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpios:Ljava/util/Map;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpiosStatus:Ljava/util/Map;

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/jetty2m/device/LedController;->gpio_current_value:I

    .line 49
    new-instance v0, Lcom/jetty2m/device/LedController$1;

    invoke-direct {v0, p0}, Lcom/jetty2m/device/LedController$1;-><init>(Lcom/jetty2m/device/LedController;)V

    iput-object v0, p0, Lcom/jetty2m/device/LedController;->handler:Landroid/os/Handler;

    .line 70
    new-instance v0, Lcom/jetty2m/device/LedController$2;

    invoke-direct {v0, p0}, Lcom/jetty2m/device/LedController$2;-><init>(Lcom/jetty2m/device/LedController;)V

    iput-object v0, p0, Lcom/jetty2m/device/LedController;->task:Ljava/util/TimerTask;

    .line 89
    new-instance v0, Lcom/jetty2m/device/GpioController;

    invoke-direct {v0}, Lcom/jetty2m/device/GpioController;-><init>()V

    iput-object v0, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/jetty2m/device/LedController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/device/LedController;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpios:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jetty2m/device/LedController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/device/LedController;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpiosStatus:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jetty2m/device/LedController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/device/LedController;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getInstance()Lcom/jetty2m/device/LedController;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/jetty2m/device/LedController;->instance:Lcom/jetty2m/device/LedController;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lcom/jetty2m/device/LedController;

    invoke-direct {v0}, Lcom/jetty2m/device/LedController;-><init>()V

    sput-object v0, Lcom/jetty2m/device/LedController;->instance:Lcom/jetty2m/device/LedController;

    .line 97
    :cond_0
    sget-object v0, Lcom/jetty2m/device/LedController;->instance:Lcom/jetty2m/device/LedController;

    return-object v0
.end method


# virtual methods
.method public setMode(II)V
    .locals 7
    .param p1, "led"    # I
    .param p2, "blink"    # I

    .prologue
    const/4 v6, 0x1

    .line 103
    const/4 v0, 0x0

    .line 104
    .local v0, "led1":I
    const/4 v2, 0x0

    .line 106
    .local v2, "led2":I
    const/4 v1, -0x1

    .line 107
    .local v1, "led1_blink":I
    const/4 v3, -0x1

    .line 109
    .local v3, "led2_blink":I
    const/16 v4, 0x3be

    if-ne p1, v4, :cond_2

    .line 110
    const/16 v0, 0x5f

    .line 111
    const/16 v2, 0x62

    .line 117
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    if-nez v2, :cond_3

    .line 119
    const/4 v4, 0x0

    invoke-virtual {p0, p1, p2, v4}, Lcom/jetty2m/device/LedController;->setModeReal(IIZ)V

    .line 139
    :cond_1
    :goto_1
    return-void

    .line 112
    :cond_2
    const/16 v4, 0x140

    if-ne p1, v4, :cond_0

    .line 113
    const/4 v0, 0x3

    .line 114
    const/4 v2, 0x2

    goto :goto_0

    .line 120
    :cond_3
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    .line 122
    iget-object v4, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 123
    iget-object v4, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 126
    :cond_4
    iget-object v4, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 127
    iget-object v4, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 130
    :cond_5
    if-ne v1, p2, :cond_6

    if-eq v3, p2, :cond_1

    .line 135
    :cond_6
    invoke-virtual {p0, v0, p2, v6}, Lcom/jetty2m/device/LedController;->setModeReal(IIZ)V

    .line 136
    invoke-virtual {p0, v2, p2, v6}, Lcom/jetty2m/device/LedController;->setModeReal(IIZ)V

    goto :goto_1
.end method

.method public setModeReal(IIZ)V
    .locals 5
    .param p1, "led"    # I
    .param p2, "blink"    # I
    .param p3, "force"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 144
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    invoke-virtual {v0, p1, v4}, Lcom/jetty2m/device/GpioController;->configGPIO(IZ)V

    .line 147
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p2, :cond_0

    if-nez p3, :cond_0

    .line 196
    :goto_0
    return-void

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    :goto_1
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpios:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpios:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpiosStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpiosStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    :cond_2
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 169
    :pswitch_0
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    invoke-virtual {v0, p1, v3}, Lcom/jetty2m/device/GpioController;->writeGPIO(IZ)V

    goto :goto_0

    .line 157
    :cond_3
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->gpioBlinkStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 172
    :pswitch_1
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    invoke-virtual {v0, p1, v4}, Lcom/jetty2m/device/GpioController;->writeGPIO(IZ)V

    goto :goto_0

    .line 175
    :pswitch_2
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpios:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpiosStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    invoke-virtual {v0, p1, v3}, Lcom/jetty2m/device/GpioController;->writeGPIO(IZ)V

    goto :goto_0

    .line 180
    :pswitch_3
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpios:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpiosStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    invoke-virtual {v0, p1, v3}, Lcom/jetty2m/device/GpioController;->writeGPIO(IZ)V

    goto/16 :goto_0

    .line 185
    :pswitch_4
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpios:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpiosStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    invoke-virtual {v0, p1, v3}, Lcom/jetty2m/device/GpioController;->writeGPIO(IZ)V

    goto/16 :goto_0

    .line 190
    :pswitch_5
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpios:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->blinkGpiosStatus:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    iget-object v0, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    invoke-virtual {v0, p1, v3}, Lcom/jetty2m/device/GpioController;->writeGPIO(IZ)V

    goto/16 :goto_0

    .line 167
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public toggleGpio(I)V
    .locals 3
    .param p1, "led"    # I

    .prologue
    .line 83
    iget-object v1, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    invoke-virtual {v1, p1}, Lcom/jetty2m/device/GpioController;->readGPIO(I)I

    move-result v0

    .line 84
    .local v0, "value":I
    iget-object v2, p0, Lcom/jetty2m/device/LedController;->mGpioController:Lcom/jetty2m/device/GpioController;

    if-gtz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, p1, v1}, Lcom/jetty2m/device/GpioController;->writeGPIO(IZ)V

    .line 85
    return-void

    .line 84
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
