.class Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "WanDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jetty2m/device/WanDataController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyPhoneStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jetty2m/device/WanDataController;


# direct methods
.method private constructor <init>(Lcom/jetty2m/device/WanDataController;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jetty2m/device/WanDataController;Lcom/jetty2m/device/WanDataController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/jetty2m/device/WanDataController;
    .param p2, "x1"    # Lcom/jetty2m/device/WanDataController$1;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;-><init>(Lcom/jetty2m/device/WanDataController;)V

    return-void
.end method


# virtual methods
.method public onDataConnectionStateChanged(II)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "networkType"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-static {v0, p2}, Lcom/jetty2m/device/WanDataController;->access$100(Lcom/jetty2m/device/WanDataController;I)V

    .line 87
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 0
    .param p1, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 92
    return-void
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 2
    .param p1, "signalStrength"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/telephony/PhoneStateListener;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V

    .line 78
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLevel()I

    move-result v1

    invoke-static {v0, v1}, Lcom/jetty2m/device/WanDataController;->access$002(Lcom/jetty2m/device/WanDataController;I)I

    .line 79
    iget-object v0, p0, Lcom/jetty2m/device/WanDataController$MyPhoneStateListener;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-virtual {v0}, Lcom/jetty2m/device/WanDataController;->updaterndisState()V

    .line 80
    return-void
.end method
