.class Lcom/jetty2m/device/DeviceController$1;
.super Landroid/content/BroadcastReceiver;
.source "DeviceController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jetty2m/device/DeviceController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jetty2m/device/DeviceController;


# direct methods
.method constructor <init>(Lcom/jetty2m/device/DeviceController;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/jetty2m/device/DeviceController$1;->this$0:Lcom/jetty2m/device/DeviceController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 47
    .local v4, "action":Ljava/lang/String;
    const-string v17, "status"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 48
    .local v12, "status":I
    const-string v17, "health"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 49
    .local v5, "health":I
    const-string v17, "present"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 50
    .local v10, "present":Z
    const-string v17, "level"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 51
    .local v7, "level":I
    const-string v17, "scale"

    const/16 v18, 0x64

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 52
    .local v11, "scale":I
    const-string v17, "plugged"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 53
    .local v9, "plugged":I
    const-string v17, "voltage"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    .line 54
    .local v16, "voltage":I
    const-string v17, "temperature"

    const/16 v18, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 55
    .local v15, "temperature":I
    const-string v17, "technology"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 56
    .local v14, "technology":Ljava/lang/String;
    const-string v13, ""

    .line 58
    .local v13, "statusString":Ljava/lang/String;
    mul-int/lit8 v17, v7, 0x64

    div-int v8, v17, v11

    .line 59
    .local v8, "percent":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/device/DeviceController$1;->this$0:Lcom/jetty2m/device/DeviceController;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v8}, Lcom/jetty2m/device/DeviceController;->access$002(Lcom/jetty2m/device/DeviceController;I)I

    .line 61
    packed-switch v12, :pswitch_data_0

    .line 91
    :goto_0
    const-string v6, ""

    .line 92
    .local v6, "healthString":Ljava/lang/String;
    packed-switch v5, :pswitch_data_1

    .line 161
    :goto_1
    const-string v3, ""

    .line 162
    .local v3, "acString":Ljava/lang/String;
    packed-switch v9, :pswitch_data_2

    .line 185
    :goto_2
    const-string v17, "android.intent.action.BATTERY_CHANGED"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 194
    :cond_0
    return-void

    .line 63
    .end local v3    # "acString":Ljava/lang/String;
    .end local v6    # "healthString":Ljava/lang/String;
    :pswitch_0
    const-string v13, "unknown"

    .line 64
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/device/DeviceController$1;->this$0:Lcom/jetty2m/device/DeviceController;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/jetty2m/device/DeviceController;->access$102(Lcom/jetty2m/device/DeviceController;Z)Z

    goto :goto_0

    .line 68
    :pswitch_1
    const-string v13, "charging"

    .line 69
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/device/DeviceController$1;->this$0:Lcom/jetty2m/device/DeviceController;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-static/range {v17 .. v18}, Lcom/jetty2m/device/DeviceController;->access$102(Lcom/jetty2m/device/DeviceController;Z)Z

    goto :goto_0

    .line 73
    :pswitch_2
    const-string v13, "discharging"

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/device/DeviceController$1;->this$0:Lcom/jetty2m/device/DeviceController;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/jetty2m/device/DeviceController;->access$102(Lcom/jetty2m/device/DeviceController;Z)Z

    goto :goto_0

    .line 78
    :pswitch_3
    const-string v13, "not charging"

    .line 79
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/device/DeviceController$1;->this$0:Lcom/jetty2m/device/DeviceController;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/jetty2m/device/DeviceController;->access$102(Lcom/jetty2m/device/DeviceController;Z)Z

    goto :goto_0

    .line 83
    :pswitch_4
    const-string v13, "full"

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jetty2m/device/DeviceController$1;->this$0:Lcom/jetty2m/device/DeviceController;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {v17 .. v18}, Lcom/jetty2m/device/DeviceController;->access$102(Lcom/jetty2m/device/DeviceController;Z)Z

    goto :goto_0

    .line 94
    .restart local v6    # "healthString":Ljava/lang/String;
    :pswitch_5
    const-string v6, "unknown"

    .line 95
    goto :goto_1

    .line 98
    :pswitch_6
    const/16 v17, 0x5

    move/from16 v0, v17

    if-ne v12, v0, :cond_2

    .line 101
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x5f

    const/16 v19, 0x1

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    .line 102
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    .line 143
    :cond_1
    :goto_3
    const-string v6, "good"

    .line 144
    goto :goto_1

    .line 104
    :cond_2
    const/16 v17, 0x2

    move/from16 v0, v17

    if-ne v12, v0, :cond_6

    .line 106
    const/16 v17, 0x5f

    move/from16 v0, v17

    if-lt v8, v0, :cond_3

    .line 107
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x5f

    const/16 v19, 0x1

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    .line 108
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto :goto_3

    .line 110
    :cond_3
    const/16 v17, 0x46

    move/from16 v0, v17

    if-lt v8, v0, :cond_4

    const/16 v17, 0x5f

    move/from16 v0, v17

    if-ge v8, v0, :cond_4

    .line 111
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x5f

    const/16 v19, 0x4

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    .line 112
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto :goto_3

    .line 114
    :cond_4
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-lt v8, v0, :cond_5

    const/16 v17, 0x46

    move/from16 v0, v17

    if-ge v8, v0, :cond_5

    .line 117
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x3be

    const/16 v19, 0x4

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto :goto_3

    .line 119
    :cond_5
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-ge v8, v0, :cond_1

    .line 120
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x5f

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    .line 121
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x4

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto :goto_3

    .line 126
    :cond_6
    const/16 v17, 0x46

    move/from16 v0, v17

    if-lt v8, v0, :cond_7

    .line 127
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x5f

    const/16 v19, 0x1

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    .line 128
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto/16 :goto_3

    .line 130
    :cond_7
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-lt v8, v0, :cond_8

    const/16 v17, 0x46

    move/from16 v0, v17

    if-ge v8, v0, :cond_8

    .line 131
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x5f

    const/16 v19, 0x1

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    .line 132
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x1

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto/16 :goto_3

    .line 135
    :cond_8
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-ge v8, v0, :cond_1

    .line 136
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x5f

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    .line 137
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x1

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto/16 :goto_3

    .line 146
    :pswitch_7
    const-string v6, "overheat"

    .line 147
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x3

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto/16 :goto_1

    .line 150
    :pswitch_8
    const-string v6, "dead"

    .line 151
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto/16 :goto_1

    .line 154
    :pswitch_9
    const-string v6, "voltage"

    .line 155
    invoke-static {}, Lcom/jetty2m/device/LedController;->getInstance()Lcom/jetty2m/device/LedController;

    move-result-object v17

    const/16 v18, 0x62

    const/16 v19, 0x3

    invoke-virtual/range {v17 .. v19}, Lcom/jetty2m/device/LedController;->setMode(II)V

    goto/16 :goto_1

    .line 158
    :pswitch_a
    const-string v6, "unspecified failure"

    goto/16 :goto_1

    .line 164
    .restart local v3    # "acString":Ljava/lang/String;
    :pswitch_b
    const-string v3, "plugged ac"

    .line 166
    goto/16 :goto_2

    .line 168
    :pswitch_c
    const-string v3, "plugged usb"

    .line 170
    goto/16 :goto_2

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 92
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 162
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
