.class public Lcom/jetty2m/device/WifiApController;
.super Ljava/lang/Object;
.source "WifiApController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jetty2m/device/WifiApController$WifiClient;
    }
.end annotation


# static fields
.field private static instance:Lcom/jetty2m/device/WifiApController;


# instance fields
.field private final WIFI_STATE_TEXTSTATE:[Ljava/lang/String;

.field private connectivityManager:Landroid/net/ConnectivityManager;

.field mContext:Landroid/content/Context;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiClients:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jetty2m/device/WifiApController$WifiClient;",
            ">;"
        }
    .end annotation
.end field

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    sput-object v0, Lcom/jetty2m/device/WifiApController;->instance:Lcom/jetty2m/device/WifiApController;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "DISABLING"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "DISABLED"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "ENABLING"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "ENABLED"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "FAILED"

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/jetty2m/device/WifiApController;->WIFI_STATE_TEXTSTATE:[Ljava/lang/String;

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/jetty2m/device/WifiApController;->mWifiClients:Ljava/util/List;

    .line 64
    new-instance v1, Lcom/jetty2m/device/WifiApController$1;

    invoke-direct {v1, p0}, Lcom/jetty2m/device/WifiApController$1;-><init>(Lcom/jetty2m/device/WifiApController;)V

    iput-object v1, p0, Lcom/jetty2m/device/WifiApController;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 126
    const-string v1, "wifi"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/jetty2m/device/WifiApController;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 127
    const-string v1, "connectivity"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    iput-object v1, p0, Lcom/jetty2m/device/WifiApController;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 128
    iput-object p1, p0, Lcom/jetty2m/device/WifiApController;->mContext:Landroid/content/Context;

    .line 130
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 131
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.TETHER_CONNECT_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 132
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 133
    const-string v1, "com.android.mifi.ap.wps.on"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    const-string v1, "com.android.mifi.ap.wps.off"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    const-string v1, "com.android.mifi.ap.wps.pbc.start"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    const-string v1, "com.android.mifi.ap.wps.pbc.timeout"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/jetty2m/device/WifiApController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/jetty2m/device/WifiApController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 139
    return-void
.end method

.method static synthetic access$000(Lcom/jetty2m/device/WifiApController;)Landroid/net/ConnectivityManager;
    .locals 1
    .param p0, "x0"    # Lcom/jetty2m/device/WifiApController;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/jetty2m/device/WifiApController;->connectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method public static getInstance()Lcom/jetty2m/device/WifiApController;
    .locals 2

    .prologue
    .line 142
    sget-object v0, Lcom/jetty2m/device/WifiApController;->instance:Lcom/jetty2m/device/WifiApController;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lcom/jetty2m/device/WifiApController;

    invoke-static {}, Lcom/jetty2m/service/MifiService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jetty2m/device/WifiApController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/jetty2m/device/WifiApController;->instance:Lcom/jetty2m/device/WifiApController;

    .line 146
    :cond_0
    sget-object v0, Lcom/jetty2m/device/WifiApController;->instance:Lcom/jetty2m/device/WifiApController;

    return-object v0
.end method


# virtual methods
.method public getClientNumber()I
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lcom/jetty2m/device/WifiApController;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherConnectedSta()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSsid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 452
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 453
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 454
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 456
    :goto_0
    return-object v1

    :cond_0
    const-string v1, " "

    goto :goto_0
.end method

.method public getWifiAPState()I
    .locals 2

    .prologue
    .line 277
    const/4 v0, -0x1

    .line 284
    .local v0, "state":I
    iget-object v1, p0, Lcom/jetty2m/device/WifiApController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v0

    .line 285
    add-int/lit8 v0, v0, -0xa

    .line 287
    return v0
.end method

.method public getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/jetty2m/device/WifiApController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public getWifiClients()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/jetty2m/device/WifiApController$WifiClient;",
            ">;"
        }
    .end annotation

    .prologue
    .line 632
    const/4 v7, 0x0

    .line 633
    .local v7, "i":I
    iget-object v0, p0, Lcom/jetty2m/device/WifiApController;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherConnectedSta()Ljava/util/List;

    move-result-object v8

    .line 635
    .local v8, "tetherList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiDevice;>;"
    iget-object v0, p0, Lcom/jetty2m/device/WifiApController;->mWifiClients:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 636
    const/4 v7, 0x0

    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 637
    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiDevice;

    .line 638
    .local v6, "device":Landroid/net/wifi/WifiDevice;
    iget v0, v6, Landroid/net/wifi/WifiDevice;->deviceState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 639
    iget-object v9, p0, Lcom/jetty2m/device/WifiApController;->mWifiClients:Ljava/util/List;

    new-instance v0, Lcom/jetty2m/device/WifiApController$WifiClient;

    iget-object v2, v6, Landroid/net/wifi/WifiDevice;->deviceName:Ljava/lang/String;

    iget-object v3, v6, Landroid/net/wifi/WifiDevice;->deviceIpAddress:Ljava/lang/String;

    iget-object v4, v6, Landroid/net/wifi/WifiDevice;->deviceAddress:Ljava/lang/String;

    const-string v5, "WIFI"

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/jetty2m/device/WifiApController$WifiClient;-><init>(Lcom/jetty2m/device/WifiApController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 643
    .end local v6    # "device":Landroid/net/wifi/WifiDevice;
    :cond_1
    iget-object v0, p0, Lcom/jetty2m/device/WifiApController;->mWifiClients:Ljava/util/List;

    return-object v0
.end method

.method public getWifiEncryptType()I
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 590
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v5

    iget-object v5, v5, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v5, v7}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    .line 591
    .local v3, "wpa":Z
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v5

    iget-object v5, v5, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    .line 593
    .local v4, "wpa2":Z
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v5

    iget-object v5, v5, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    .line 594
    .local v0, "ccmp":Z
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v5

    iget-object v5, v5, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v5, v7}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    .line 596
    .local v2, "tkip":Z
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 597
    const/4 v1, 0x1

    .line 608
    .local v1, "crypt":I
    :goto_0
    return v1

    .line 598
    .end local v1    # "crypt":I
    :cond_0
    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 599
    const/4 v1, 0x2

    .restart local v1    # "crypt":I
    goto :goto_0

    .line 600
    .end local v1    # "crypt":I
    :cond_1
    if-eqz v4, :cond_2

    if-eqz v2, :cond_2

    .line 601
    const/4 v1, 0x3

    .restart local v1    # "crypt":I
    goto :goto_0

    .line 602
    .end local v1    # "crypt":I
    :cond_2
    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    .line 603
    const/4 v1, 0x4

    .restart local v1    # "crypt":I
    goto :goto_0

    .line 605
    .end local v1    # "crypt":I
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "crypt":I
    goto :goto_0
.end method

.method public getWifiHwMode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 541
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 542
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->hwMode:Ljava/lang/String;

    .line 544
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "n"

    goto :goto_0
.end method

.method public getWifiMAC()Ljava/lang/String;
    .locals 8

    .prologue
    .line 473
    const-string v3, ""

    .line 475
    .local v3, "mac":Ljava/lang/String;
    :try_start_0
    const-string v5, "sys/class/net/wlan0/address"

    .line 476
    .local v5, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 477
    .local v2, "fis":Ljava/io/FileInputStream;
    const/16 v6, 0x40

    new-array v0, v6, [B

    .line 478
    .local v0, "buffer":[B
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v1

    .line 479
    .local v1, "byteCount":I
    if-lez v1, :cond_0

    .line 481
    new-instance v4, Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "utf-8"

    invoke-direct {v4, v0, v6, v1, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .end local v3    # "mac":Ljava/lang/String;
    .local v4, "mac":Ljava/lang/String;
    move-object v3, v4

    .line 483
    .end local v4    # "mac":Ljava/lang/String;
    .restart local v3    # "mac":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    if-nez v3, :cond_2

    .line 484
    :cond_1
    const-string v6, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 490
    .end local v0    # "buffer":[B
    .end local v1    # "byteCount":I
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "path":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 486
    :catch_0
    move-exception v6

    .line 490
    :cond_2
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public getWifiMaxSta()I
    .locals 2

    .prologue
    .line 562
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 563
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 564
    iget v1, v0, Landroid/net/wifi/WifiConfiguration;->maxSta:I

    .line 566
    :goto_0
    return v1

    :cond_0
    const/16 v1, 0xa

    goto :goto_0
.end method

.method public getWifiSsidHidden()Z
    .locals 2

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 498
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 499
    iget-boolean v1, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    .line 501
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWlanDHCP_High()Ljava/lang/String;
    .locals 1

    .prologue
    .line 433
    const-string v0, "192.168.43.254"

    return-object v0
.end method

.method public getWlanDHCP_Low()Ljava/lang/String;
    .locals 1

    .prologue
    .line 443
    const-string v0, "192.168.43.2"

    return-object v0
.end method

.method public getWlanDNS()Ljava/lang/String;
    .locals 11

    .prologue
    .line 382
    const-string v5, "8.8.8.8"

    .line 383
    .local v5, "response":Ljava/lang/String;
    const-string v0, "getprop net.rmnet_data0.dns1"

    .line 385
    .local v0, "command":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v4

    .line 386
    .local v4, "process":Ljava/lang/Process;
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v4}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 387
    .local v3, "ir":Ljava/io/InputStreamReader;
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 389
    .local v2, "input":Ljava/io/BufferedReader;
    new-instance v6, Ljava/lang/StringBuffer;

    const-string v8, ""

    invoke-direct {v6, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 391
    .local v6, "sb":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .local v7, "temp":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 392
    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 399
    .end local v2    # "input":Ljava/io/BufferedReader;
    .end local v3    # "ir":Ljava/io/InputStreamReader;
    .end local v4    # "process":Ljava/lang/Process;
    .end local v6    # "sb":Ljava/lang/StringBuffer;
    .end local v7    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 400
    .local v1, "e":Ljava/lang/Exception;
    const-string v8, "WifiApController"

    const-string v9, "get DNS error"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    const-string v8, "WifiApController"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "get DNS response: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    return-object v5

    .line 394
    .restart local v2    # "input":Ljava/io/BufferedReader;
    .restart local v3    # "ir":Ljava/io/InputStreamReader;
    .restart local v4    # "process":Ljava/lang/Process;
    .restart local v6    # "sb":Ljava/lang/StringBuffer;
    .restart local v7    # "temp":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 395
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    .line 396
    const-string v8, ""

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 397
    const-string v5, "8.8.8.8"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public getWlanIP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    const-string v0, "192.168.43.1"

    return-object v0
.end method

.method public getWlanStatus()Z
    .locals 2

    .prologue
    .line 368
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 369
    const/4 v0, 0x1

    .line 371
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restartAP()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 656
    const-string v0, "WifiApController"

    const-string v1, "restartAP "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/jetty2m/device/WifiApController;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)I

    .line 659
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, Lcom/jetty2m/device/WifiApController;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)I

    .line 660
    return-void
.end method

.method public setSsid(Ljava/lang/String;)V
    .locals 4
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 462
    const-string v1, "WifiApController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSsid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 464
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 465
    iput-object p1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 466
    invoke-virtual {p0, v0}, Lcom/jetty2m/device/WifiApController;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z

    .line 469
    :cond_0
    return-void
.end method

.method public setWifiApConfig(Ljava/lang/String;ILjava/lang/String;)Z
    .locals 7
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "pwtype"    # I
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 200
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 201
    .local v0, "config":Landroid/net/wifi/WifiConfiguration;
    iput-object p1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 203
    packed-switch p2, :pswitch_data_0

    .line 272
    :goto_0
    return v1

    .line 205
    :pswitch_0
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 206
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->set(I)V

    .line 271
    :cond_0
    :goto_1
    invoke-virtual {p0, v0}, Lcom/jetty2m/device/WifiApController;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z

    move v1, v2

    .line 272
    goto :goto_0

    .line 210
    :pswitch_1
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 211
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3, v2}, Ljava/util/BitSet;->set(I)V

    .line 212
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 213
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->set(I)V

    .line 214
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 215
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    .line 216
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 217
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 218
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    iput-object p3, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto :goto_1

    .line 224
    :pswitch_2
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 225
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3, v2}, Ljava/util/BitSet;->set(I)V

    .line 226
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 227
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->set(I)V

    .line 228
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 229
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v5}, Ljava/util/BitSet;->set(I)V

    .line 230
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 231
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    .line 232
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 233
    iput-object p3, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto :goto_1

    .line 238
    :pswitch_3
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 239
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V

    .line 240
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 241
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->set(I)V

    .line 242
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 243
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    .line 244
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 245
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 246
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    iput-object p3, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto/16 :goto_1

    .line 252
    :pswitch_4
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 253
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V

    .line 254
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 255
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->set(I)V

    .line 256
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 257
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v5}, Ljava/util/BitSet;->set(I)V

    .line 258
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 259
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    .line 260
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    iput-object p3, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto/16 :goto_1

    .line 203
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z
    .locals 1
    .param p1, "wifiConfig"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/jetty2m/device/WifiApController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z

    .line 176
    const/4 v0, 0x1

    return v0
.end method

.method public setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)I
    .locals 10
    .param p1, "config"    # Landroid/net/wifi/WifiConfiguration;
    .param p2, "enabled"    # Z

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x1

    .line 297
    const-string v4, "WifiApController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "*** setWifiApEnabled CALLED **** "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    if-eqz p2, :cond_0

    iget-object v4, p0, Lcom/jetty2m/device/WifiApController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 299
    iget-object v4, p0, Lcom/jetty2m/device/WifiApController;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 301
    const-wide/16 v4, 0x5dc

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    :cond_0
    :goto_0
    const/4 v3, -0x1

    .line 320
    .local v3, "state":I
    iget-object v4, p0, Lcom/jetty2m/device/WifiApController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4, p1, p2}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    move-result v2

    .line 323
    .local v2, "result":Z
    if-nez p2, :cond_5

    .line 324
    const/16 v1, 0x1e

    .line 325
    .local v1, "loopMax":I
    :goto_1
    if-lez v1, :cond_2

    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v4

    if-eq v4, v8, :cond_1

    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v4

    if-ne v4, v9, :cond_2

    .line 329
    :cond_1
    const-wide/16 v4, 0x1f4

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 330
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 334
    :cond_2
    iget-object v4, p0, Lcom/jetty2m/device/WifiApController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4, v7}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 349
    .end local v1    # "loopMax":I
    :cond_3
    iget-object v4, p0, Lcom/jetty2m/device/WifiApController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 358
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v4

    if-ne v4, v8, :cond_4

    .line 364
    :cond_4
    return v3

    .line 335
    .end local v0    # "conf":Landroid/net/wifi/WifiConfiguration;
    :cond_5
    if-eqz p2, :cond_3

    .line 336
    const/16 v1, 0x1e

    .line 337
    .restart local v1    # "loopMax":I
    :goto_2
    if-lez v1, :cond_3

    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_6

    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v4

    if-eq v4, v7, :cond_6

    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v4

    if-ne v4, v9, :cond_3

    .line 341
    :cond_6
    const-wide/16 v4, 0x1f4

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 342
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 302
    .end local v1    # "loopMax":I
    .end local v2    # "result":Z
    .end local v3    # "state":I
    :catch_0
    move-exception v4

    goto :goto_0

    .line 331
    .restart local v1    # "loopMax":I
    .restart local v2    # "result":Z
    .restart local v3    # "state":I
    :catch_1
    move-exception v4

    goto :goto_1

    .line 343
    :catch_2
    move-exception v4

    goto :goto_2
.end method

.method public setWifiHwMode(Ljava/lang/String;)V
    .locals 4
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 549
    const-string v1, "WifiApController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWifiHwMode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 552
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 553
    iput-object p1, v0, Landroid/net/wifi/WifiConfiguration;->hwMode:Ljava/lang/String;

    .line 554
    invoke-virtual {p0, v0}, Lcom/jetty2m/device/WifiApController;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z

    .line 558
    :cond_0
    return-void
.end method

.method public setWifiMaxSta(I)V
    .locals 4
    .param p1, "maxsta"    # I

    .prologue
    .line 571
    const-string v1, "WifiApController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWifiMaxSta "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 574
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 575
    iput p1, v0, Landroid/net/wifi/WifiConfiguration;->maxSta:I

    .line 576
    invoke-virtual {p0, v0}, Lcom/jetty2m/device/WifiApController;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z

    .line 580
    :cond_0
    return-void
.end method

.method public setWifiSsidHidden(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 506
    const-string v1, "WifiApController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWifiSsidHidden "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    invoke-virtual {p0}, Lcom/jetty2m/device/WifiApController;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 508
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 509
    iput-boolean p1, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    .line 510
    invoke-virtual {p0, v0}, Lcom/jetty2m/device/WifiApController;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z

    .line 514
    :cond_0
    return-void
.end method

.method public setWlanDns(Ljava/lang/String;)V
    .locals 10
    .param p1, "dns"    # Ljava/lang/String;

    .prologue
    .line 409
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getprop net.rmnet_data0.dns1 "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 411
    .local v0, "command":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v4

    .line 412
    .local v4, "process":Ljava/lang/Process;
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v4}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 413
    .local v3, "ir":Ljava/io/InputStreamReader;
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 415
    .local v2, "input":Ljava/io/BufferedReader;
    new-instance v5, Ljava/lang/StringBuffer;

    const-string v7, ""

    invoke-direct {v5, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 417
    .local v5, "sb":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .local v6, "temp":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 418
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 424
    .end local v2    # "input":Ljava/io/BufferedReader;
    .end local v3    # "ir":Ljava/io/InputStreamReader;
    .end local v4    # "process":Ljava/lang/Process;
    .end local v5    # "sb":Ljava/lang/StringBuffer;
    .end local v6    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 425
    .local v1, "e":Ljava/lang/Exception;
    const-string v7, "WifiApController"

    const-string v8, "get DNS error"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 420
    .restart local v2    # "input":Ljava/io/BufferedReader;
    .restart local v3    # "ir":Ljava/io/InputStreamReader;
    .restart local v4    # "process":Ljava/lang/Process;
    .restart local v5    # "sb":Ljava/lang/StringBuffer;
    .restart local v6    # "temp":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 422
    const-string v7, "WifiApController"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "set DNS ok: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
