.class Lcom/jetty2m/device/WifiApController$1;
.super Landroid/content/BroadcastReceiver;
.source "WifiApController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jetty2m/device/WifiApController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jetty2m/device/WifiApController;


# direct methods
.method constructor <init>(Lcom/jetty2m/device/WifiApController;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/jetty2m/device/WifiApController$1;->this$0:Lcom/jetty2m/device/WifiApController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 67
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.net.conn.TETHER_CONNECT_STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 70
    iget-object v4, p0, Lcom/jetty2m/device/WifiApController$1;->this$0:Lcom/jetty2m/device/WifiApController;

    invoke-static {v4}, Lcom/jetty2m/device/WifiApController;->access$000(Lcom/jetty2m/device/WifiApController;)Landroid/net/ConnectivityManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getTetherConnectedSta()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 71
    .local v1, "isConnected":Z
    :goto_0
    iget-object v3, p0, Lcom/jetty2m/device/WifiApController$1;->this$0:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 121
    .end local v1    # "isConnected":Z
    :cond_0
    :goto_1
    :pswitch_0
    return-void

    :cond_1
    move v1, v3

    .line 70
    goto :goto_0

    .line 80
    :cond_2
    const-string v4, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 81
    const-string v4, "wifi_state"

    const/16 v5, 0xe

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 82
    .local v2, "state":I
    iget-object v4, p0, Lcom/jetty2m/device/WifiApController$1;->this$0:Lcom/jetty2m/device/WifiApController;

    invoke-static {v4}, Lcom/jetty2m/device/WifiApController;->access$000(Lcom/jetty2m/device/WifiApController;)Landroid/net/ConnectivityManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getTetherConnectedSta()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 83
    .restart local v1    # "isConnected":Z
    :goto_2
    packed-switch v2, :pswitch_data_0

    :pswitch_1
    goto :goto_1

    .end local v1    # "isConnected":Z
    :cond_3
    move v1, v3

    .line 82
    goto :goto_2

    .line 99
    .end local v2    # "state":I
    :cond_4
    const-string v4, "com.android.mifi.ap.wps.on"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 102
    const-string v4, "com.android.mifi.ap.wps.off"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 105
    const-string v4, "com.android.mifi.ap.wps.pbc.start"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 108
    const-string v4, "com.android.mifi.ap.wps.pbc.timeout"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 110
    iget-object v4, p0, Lcom/jetty2m/device/WifiApController$1;->this$0:Lcom/jetty2m/device/WifiApController;

    invoke-static {v4}, Lcom/jetty2m/device/WifiApController;->access$000(Lcom/jetty2m/device/WifiApController;)Landroid/net/ConnectivityManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getTetherConnectedSta()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_5

    .line 111
    .restart local v1    # "isConnected":Z
    :goto_3
    iget-object v3, p0, Lcom/jetty2m/device/WifiApController$1;->this$0:Lcom/jetty2m/device/WifiApController;

    invoke-virtual {v3}, Lcom/jetty2m/device/WifiApController;->getWifiAPState()I

    move-result v3

    if-ne v3, v5, :cond_0

    goto :goto_1

    .end local v1    # "isConnected":Z
    :cond_5
    move v1, v3

    .line 110
    goto :goto_3

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
