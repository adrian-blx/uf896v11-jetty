.class public Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;
.super Landroid/content/BroadcastReceiver;
.source "WanDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jetty2m/device/WanDataController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NetWorkBroadcast"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jetty2m/device/WanDataController;


# direct methods
.method public constructor <init>(Lcom/jetty2m/device/WanDataController;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x1

    .line 365
    const/4 v3, 0x0

    .line 366
    .local v3, "network_type":I
    const/4 v1, 0x0

    .line 368
    .local v1, "connected":Z
    const-string v6, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 369
    const-string v6, "WanDataController"

    const-string v7, "ConnectivityManager CONNECTIVITY_ACTION"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    iget-object v6, p0, Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-static {v6}, Lcom/jetty2m/device/WanDataController;->access$300(Lcom/jetty2m/device/WanDataController;)Landroid/net/ConnectivityManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 372
    .local v0, "activeNetInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v6

    if-nez v6, :cond_3

    .line 374
    :cond_0
    const/4 v3, 0x0

    .line 375
    const/4 v1, 0x0

    .line 398
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    .line 400
    sget v6, Lcom/jetty2m/device/WanDataController;->UsbTethering_enabled:I

    if-nez v6, :cond_2

    .line 401
    iget-object v6, p0, Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-virtual {v6, v9}, Lcom/jetty2m/device/WanDataController;->setUsbTethering(Z)Z

    .line 402
    sput v9, Lcom/jetty2m/device/WanDataController;->UsbTethering_enabled:I

    .line 410
    .end local v0    # "activeNetInfo":Landroid/net/NetworkInfo;
    :cond_2
    return-void

    .line 379
    .restart local v0    # "activeNetInfo":Landroid/net/NetworkInfo;
    :cond_3
    iget-object v6, p0, Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-static {v6}, Lcom/jetty2m/device/WanDataController;->access$300(Lcom/jetty2m/device/WanDataController;)Landroid/net/ConnectivityManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 380
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    .line 381
    const-string v6, "WanDataController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ConnectivityManager getNetworkType "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-static {v8}, Lcom/jetty2m/device/WanDataController;->access$400(Lcom/jetty2m/device/WanDataController;)Landroid/telephony/TelephonyManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v4

    .line 383
    .local v4, "state":Landroid/net/NetworkInfo$State;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v5

    .line 385
    .local v5, "strSubTypeName":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 387
    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v4, v6, :cond_4

    .line 388
    const/4 v1, 0x1

    .line 391
    :cond_4
    iget-object v6, p0, Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;->this$0:Lcom/jetty2m/device/WanDataController;

    iget-object v7, p0, Lcom/jetty2m/device/WanDataController$NetWorkBroadcast;->this$0:Lcom/jetty2m/device/WanDataController;

    invoke-static {v7}, Lcom/jetty2m/device/WanDataController;->access$400(Lcom/jetty2m/device/WanDataController;)Landroid/telephony/TelephonyManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v7

    invoke-static {v6, v7}, Lcom/jetty2m/device/WanDataController;->access$100(Lcom/jetty2m/device/WanDataController;I)V

    goto :goto_0
.end method
