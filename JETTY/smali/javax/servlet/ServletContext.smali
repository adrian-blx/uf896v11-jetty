.class public interface abstract Ljavax/servlet/ServletContext;
.super Ljava/lang/Object;
.source "ServletContext.java"


# virtual methods
.method public abstract getAttribute(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method public abstract getContextPath()Ljava/lang/String;
.end method

.method public abstract getRequestDispatcher(Ljava/lang/String;)Ljavax/servlet/RequestDispatcher;
.end method

.method public abstract log(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method
