.class public interface abstract Ljavax/servlet/ServletResponse;
.super Ljava/lang/Object;
.source "ServletResponse.java"


# virtual methods
.method public abstract getCharacterEncoding()Ljava/lang/String;
.end method

.method public abstract getOutputStream()Ljavax/servlet/ServletOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getWriter()Ljava/io/PrintWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract isCommitted()Z
.end method

.method public abstract resetBuffer()V
.end method

.method public abstract setCharacterEncoding(Ljava/lang/String;)V
.end method

.method public abstract setContentLength(I)V
.end method

.method public abstract setContentType(Ljava/lang/String;)V
.end method
