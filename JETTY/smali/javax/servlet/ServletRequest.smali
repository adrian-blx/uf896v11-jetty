.class public interface abstract Ljavax/servlet/ServletRequest;
.super Ljava/lang/Object;
.source "ServletRequest.java"


# virtual methods
.method public abstract getAsyncContext()Ljavax/servlet/AsyncContext;
.end method

.method public abstract getAttribute(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method public abstract getContentType()Ljava/lang/String;
.end method

.method public abstract getInputStream()Ljavax/servlet/ServletInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getParameter(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getProtocol()Ljava/lang/String;
.end method

.method public abstract getRequestDispatcher(Ljava/lang/String;)Ljavax/servlet/RequestDispatcher;
.end method

.method public abstract getServerName()Ljava/lang/String;
.end method

.method public abstract getServletContext()Ljavax/servlet/ServletContext;
.end method

.method public abstract isAsyncStarted()Z
.end method

.method public abstract isSecure()Z
.end method

.method public abstract setAttribute(Ljava/lang/String;Ljava/lang/Object;)V
.end method
