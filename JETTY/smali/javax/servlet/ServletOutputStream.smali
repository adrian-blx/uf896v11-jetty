.class public abstract Ljavax/servlet/ServletOutputStream;
.super Ljava/io/OutputStream;
.source "ServletOutputStream.java"


# static fields
.field private static lStrings:Ljava/util/ResourceBundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const-string v0, "javax.servlet.LocalStrings"

    invoke-static {v0}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;)Ljava/util/ResourceBundle;

    move-result-object v0

    sput-object v0, Ljavax/servlet/ServletOutputStream;->lStrings:Ljava/util/ResourceBundle;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    return-void
.end method


# virtual methods
.method public print(Ljava/lang/String;)V
    .locals 7
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    if-nez p1, :cond_0

    const-string p1, "null"

    .line 115
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 116
    .local v4, "len":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 117
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 125
    .local v0, "c":C
    const v5, 0xff00

    and-int/2addr v5, v0

    if-eqz v5, :cond_1

    .line 126
    sget-object v5, Ljavax/servlet/ServletOutputStream;->lStrings:Ljava/util/ResourceBundle;

    const-string v6, "err.not_iso8859_1"

    invoke-virtual {v5, v6}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 127
    .local v2, "errMsg":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v1, v5, [Ljava/lang/Object;

    .line 128
    .local v1, "errArgs":[Ljava/lang/Object;
    const/4 v5, 0x0

    new-instance v6, Ljava/lang/Character;

    invoke-direct {v6, v0}, Ljava/lang/Character;-><init>(C)V

    aput-object v6, v1, v5

    .line 129
    invoke-static {v2, v1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 130
    new-instance v5, Ljava/io/CharConversionException;

    invoke-direct {v5, v2}, Ljava/io/CharConversionException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 132
    .end local v1    # "errArgs":[Ljava/lang/Object;
    .end local v2    # "errMsg":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, v0}, Ljavax/servlet/ServletOutputStream;->write(I)V

    .line 116
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 134
    .end local v0    # "c":C
    :cond_2
    return-void
.end method
