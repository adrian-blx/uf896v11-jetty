.class public interface abstract Ljavax/servlet/http/HttpServletRequest;
.super Ljava/lang/Object;
.source "HttpServletRequest.java"

# interfaces
.implements Ljavax/servlet/ServletRequest;


# virtual methods
.method public abstract getContextPath()Ljava/lang/String;
.end method

.method public abstract getCookies()[Ljavax/servlet/http/Cookie;
.end method

.method public abstract getDateHeader(Ljava/lang/String;)J
.end method

.method public abstract getHeader(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getHeaderNames()Ljava/util/Enumeration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMethod()Ljava/lang/String;
.end method

.method public abstract getPathInfo()Ljava/lang/String;
.end method

.method public abstract getQueryString()Ljava/lang/String;
.end method

.method public abstract getRequestURI()Ljava/lang/String;
.end method

.method public abstract getRequestURL()Ljava/lang/StringBuffer;
.end method

.method public abstract getRequestedSessionId()Ljava/lang/String;
.end method

.method public abstract getServletPath()Ljava/lang/String;
.end method

.method public abstract getSession(Z)Ljavax/servlet/http/HttpSession;
.end method
