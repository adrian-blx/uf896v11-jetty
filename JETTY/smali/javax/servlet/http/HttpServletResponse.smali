.class public interface abstract Ljavax/servlet/http/HttpServletResponse;
.super Ljava/lang/Object;
.source "HttpServletResponse.java"

# interfaces
.implements Ljavax/servlet/ServletResponse;


# virtual methods
.method public abstract containsHeader(Ljava/lang/String;)Z
.end method

.method public abstract encodeRedirectURL(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract sendError(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract sendError(ILjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract sendRedirect(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setDateHeader(Ljava/lang/String;J)V
.end method

.method public abstract setHeader(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setStatus(I)V
.end method
